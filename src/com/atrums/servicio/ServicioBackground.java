package com.atrums.servicio;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.atrums.dao.OperacionesDomicilios;

public class ServicioBackground extends HttpServlet implements Servlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static final Logger log = Logger.getLogger(ServicioBackground.class);
	
	public ServicioBackground(){
        super();
    }

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
	
	@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
	
	@Override
    public String getServletInfo() {
        return "Short description";
    }
	
	@Override
    public void init(ServletConfig config) throws ServletException {
		log.info("Ejecutando servicio de domicilios");
		
		try {
			ExecutorService domicilios = Executors.newCachedThreadPool();
			Runnable runnable = new OperacionesDomicilios();
			domicilios.execute(runnable);
		} catch (Exception ex) {
			// TODO: handle exception
			log.warn(ex.getMessage());
		}
    }
}
