package com.atrums.modelo;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class Configuracion {
	static final Logger log = Logger.getLogger(Configuracion.class);
	private InputStream confService = null;
	Properties propiedad = new Properties();
	
	/**
	 * Postgres Server
	 * **/
	
	private String ipsevidor;
	private String puertoservidor;
	private String usuarioservidor;
	private String passwordservidor;
	private String bddsevidor;
	
	/**
	 * Oracle Server
	 * **/
	
	private String iporacle;
	private String puertooracle;
	private String usuariooracle;
	private String passwordoracle;
	private String bddoracle;
	
	/**
	 * Empresa
	 * **/
	
	private String empresa;
	private String categoriaproducto;
	
	public Configuracion() {
		try {
			this.confService = this.getClass().getClassLoader().getResourceAsStream("./..//CONFIGURACION/configuracion.xml");
			this.propiedad.loadFromXML(confService);
			
			/**
			 * Postgres Server
			 * **/
			
			this.ipsevidor = this.propiedad.getProperty("ipservidor");
			this.puertoservidor = this.propiedad.getProperty("puertoservidor");
			this.usuarioservidor = this.propiedad.getProperty("usuariosservidor");
			this.passwordservidor = this.propiedad.getProperty("passwordservidor");
			this.bddsevidor = this.propiedad.getProperty("bddservidor");
			
			/**
			 * Oracle Server
			 * **/
			
			this.iporacle = this.propiedad.getProperty("iporacle");
			this.puertooracle = this.propiedad.getProperty("puertooracle");
			this.usuariooracle = this.propiedad.getProperty("usuariooracle");
			this.passwordoracle = this.propiedad.getProperty("passwordoracle");
			this.bddoracle = this.propiedad.getProperty("bddoracle");
			
			/**
			 * Empresa
			 * **/
			
			this.empresa = this.propiedad.getProperty("empresa");
			this.categoriaproducto = this.propiedad.getProperty("categoriaproducto");
			
			this.propiedad.clear();
			this.confService.close();
		} catch (IOException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		}
	}

	public String getCategoriaproducto() {
		return categoriaproducto;
	}

	public void setCategoriaproducto(String categoriaproducto) {
		this.categoriaproducto = categoriaproducto;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getIpsevidor() {
		return ipsevidor;
	}

	public String getIporacle() {
		return iporacle;
	}

	public void setIporacle(String iporacle) {
		this.iporacle = iporacle;
	}

	public String getPuertooracle() {
		return puertooracle;
	}

	public void setPuertooracle(String puertooracle) {
		this.puertooracle = puertooracle;
	}

	public String getUsuariooracle() {
		return usuariooracle;
	}

	public void setUsuariooracle(String usuariooracle) {
		this.usuariooracle = usuariooracle;
	}

	public String getPasswordoracle() {
		return passwordoracle;
	}

	public void setPasswordoracle(String passwordoracle) {
		this.passwordoracle = passwordoracle;
	}

	public String getBddoracle() {
		return bddoracle;
	}

	public void setBddoracle(String bddoracle) {
		this.bddoracle = bddoracle;
	}

	public void setIpsevidor(String ipsevidor) {
		this.ipsevidor = ipsevidor;
	}

	public String getPuertoservidor() {
		return puertoservidor;
	}

	public void setPuertoservidor(String puertoservidor) {
		this.puertoservidor = puertoservidor;
	}

	public String getUsuarioservidor() {
		return usuarioservidor;
	}

	public void setUsuarioservidor(String usuarioservidor) {
		this.usuarioservidor = usuarioservidor;
	}

	public String getPasswordservidor() {
		return passwordservidor;
	}

	public void setPasswordservidor(String passwordservidor) {
		this.passwordservidor = passwordservidor;
	}

	public String getBddsevidor() {
		return bddsevidor;
	}

	public void setBddsevidor(String bddsevidor) {
		this.bddsevidor = bddsevidor;
	}	
}
