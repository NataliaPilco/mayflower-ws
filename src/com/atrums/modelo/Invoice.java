package com.atrums.modelo;

import org.apache.log4j.Logger;

public class Invoice {
	static final Logger log = Logger.getLogger(Invoice.class);
	
	private String cInvoiceId;
	private String adClientId;
	private String adOrgId;
	private String cDoctypeId;
	private String cBpartnerId;
	private String adUserId;
	private String cLocationId;
	private String cBpartnerLocationId;
	
	private String claveacceso;
	private String documentno;
	private String sucursal;
	private String ptoemision;
	private String dateinvoiced;
	private String description;
	private String autorization;
	private String docXML;
	private double servicio;
	private String total;
	
	/**
	 * Variables de procesamiento;
	 * **/
	private String v_BPartner_ID ;
	private String v_DocAction ;
	private String v_DocStatus ;
	private String v_bpartner_blocked ;
	private String v_invoiceBlocking ;
	private String v_UpdatedBy ;
	private String v_Org_ID ;
	private String v_DateAcct ;
	private String v_DocTypeTarget_ID ;
	private String v_istaxincluded = "N";
	private String v_IsSOTrx = "N";
	private String v_Client_ID ;
	private String v_DocumentNo ;
	private String v_BPartner_User_ID ;
	private int v_nrodumentos = 0;
	
	private String v_is_ready = "N";
	private String v_is_tr_allow = "N";
	private int v_is_included = 0;
	
	private String v_isacctle = "N";
	
	private int v_count = 0;
	private int v_Count_2 = 0;
	private int v_Count_3 = 0;
	private int v_count_4 = 0;
	
	private int v_available_period = 0;
	
	public Invoice() {
		super();
	}

	public String getV_DocAction() {
		return v_DocAction;
	}

	public void setV_DocAction(String v_DocAction) {
		this.v_DocAction = v_DocAction;
	}

	public String getV_DocStatus() {
		return v_DocStatus;
	}

	public void setV_DocStatus(String v_DocStatus) {
		this.v_DocStatus = v_DocStatus;
	}

	public String getV_bpartner_blocked() {
		return v_bpartner_blocked;
	}

	public void setV_bpartner_blocked(String v_bpartner_blocked) {
		this.v_bpartner_blocked = v_bpartner_blocked;
	}

	public String getV_invoiceBlocking() {
		return v_invoiceBlocking;
	}

	public void setV_invoiceBlocking(String v_invoiceBlocking) {
		this.v_invoiceBlocking = v_invoiceBlocking;
	}

	public String getV_UpdatedBy() {
		return v_UpdatedBy;
	}

	public void setV_UpdatedBy(String v_UpdatedBy) {
		this.v_UpdatedBy = v_UpdatedBy;
	}

	public String getV_Org_ID() {
		return v_Org_ID;
	}

	public void setV_Org_ID(String v_Org_ID) {
		this.v_Org_ID = v_Org_ID;
	}

	public String getV_DateAcct() {
		return v_DateAcct;
	}

	public void setV_DateAcct(String v_DateAcct) {
		this.v_DateAcct = v_DateAcct;
	}

	public String getV_DocTypeTarget_ID() {
		return v_DocTypeTarget_ID;
	}

	public void setV_DocTypeTarget_ID(String v_DocTypeTarget_ID) {
		this.v_DocTypeTarget_ID = v_DocTypeTarget_ID;
	}

	public String getV_istaxincluded() {
		return v_istaxincluded;
	}

	public void setV_istaxincluded(String v_istaxincluded) {
		this.v_istaxincluded = v_istaxincluded;
	}

	public String getV_IsSOTrx() {
		return v_IsSOTrx;
	}

	public void setV_IsSOTrx(String v_IsSOTrx) {
		this.v_IsSOTrx = v_IsSOTrx;
	}

	public String getV_Client_ID() {
		return v_Client_ID;
	}

	public void setV_Client_ID(String v_Client_ID) {
		this.v_Client_ID = v_Client_ID;
	}

	public String getV_DocumentNo() {
		return v_DocumentNo;
	}

	public void setV_DocumentNo(String v_DocumentNo) {
		this.v_DocumentNo = v_DocumentNo;
	}

	public String getV_BPartner_User_ID() {
		return v_BPartner_User_ID;
	}

	public void setV_BPartner_User_ID(String v_BPartner_User_ID) {
		this.v_BPartner_User_ID = v_BPartner_User_ID;
	}

	public int getV_nrodumentos() {
		return v_nrodumentos;
	}

	public void setV_nrodumentos(int v_nrodumentos) {
		this.v_nrodumentos = v_nrodumentos;
	}

	public String getV_is_ready() {
		return v_is_ready;
	}

	public void setV_is_ready(String v_is_ready) {
		this.v_is_ready = v_is_ready;
	}

	public String getV_is_tr_allow() {
		return v_is_tr_allow;
	}

	public void setV_is_tr_allow(String v_is_tr_allow) {
		this.v_is_tr_allow = v_is_tr_allow;
	}

	public int getV_is_included() {
		return v_is_included;
	}

	public void setV_is_included(int v_is_included) {
		this.v_is_included = v_is_included;
	}

	public String getV_isacctle() {
		return v_isacctle;
	}

	public void setV_isacctle(String v_isacctle) {
		this.v_isacctle = v_isacctle;
	}

	public int getV_count() {
		return v_count;
	}

	public void setV_count(int v_count) {
		this.v_count = v_count;
	}

	public int getV_Count_2() {
		return v_Count_2;
	}

	public void setV_Count_2(int v_Count_2) {
		this.v_Count_2 = v_Count_2;
	}

	public int getV_Count_3() {
		return v_Count_3;
	}

	public void setV_Count_3(int v_Count_3) {
		this.v_Count_3 = v_Count_3;
	}

	public int getV_count_4() {
		return v_count_4;
	}

	public void setV_count_4(int v_count_4) {
		this.v_count_4 = v_count_4;
	}

	public int getV_available_period() {
		return v_available_period;
	}

	public void setV_available_period(int v_available_period) {
		this.v_available_period = v_available_period;
	}

	public String getV_BPartner_ID() {
		return v_BPartner_ID;
	}

	public void setV_BPartner_ID(String v_BPartner_ID) {
		this.v_BPartner_ID = v_BPartner_ID;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public double getServicio() {
		return servicio;
	}

	public void setServicio(double servicio) {
		this.servicio = servicio;
	}

	public String getcInvoiceId() {
		return cInvoiceId;
	}

	public void setcInvoiceId(String cInvoiceId) {
		this.cInvoiceId = cInvoiceId;
	}

	public String getDocXML() {
		return docXML;
	}

	public void setDocXML(String docXML) {
		this.docXML = docXML;
	}

	public String getAutorization() {
		return autorization;
	}

	public void setAutorization(String autorization) {
		this.autorization = autorization;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDateinvoiced() {
		return dateinvoiced;
	}

	public void setDateinvoiced(String dateinvoiced) {
		this.dateinvoiced = dateinvoiced;
	}

	public String getcBpartnerLocationId() {
		return cBpartnerLocationId;
	}

	public void setcBpartnerLocationId(String cBpartnerLocationId) {
		this.cBpartnerLocationId = cBpartnerLocationId;
	}

	public String getcLocationId() {
		return cLocationId;
	}

	public void setcLocationId(String cLocationId) {
		this.cLocationId = cLocationId;
	}

	public String getAdUserId() {
		return adUserId;
	}

	public void setAdUserId(String adUserId) {
		this.adUserId = adUserId;
	}

	public String getcBpartnerId() {
		return cBpartnerId;
	}

	public void setcBpartnerId(String cBpartnerId) {
		this.cBpartnerId = cBpartnerId;
	}

	public String getcDoctypeId() {
		return cDoctypeId;
	}

	public void setcDoctypeId(String cDoctypeId) {
		this.cDoctypeId = cDoctypeId;
	}

	public String getAdOrgId() {
		return adOrgId;
	}

	public void setAdOrgId(String adOrgId) {
		this.adOrgId = adOrgId;
	}

	public String getSucursal() {
		return sucursal;
	}

	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	public String getPtoemision() {
		return ptoemision;
	}

	public void setPtoemision(String ptoemision) {
		this.ptoemision = ptoemision;
	}

	public String getDocumentno() {
		return documentno;
	}

	public void setDocumentno(String documentno) {
		this.documentno = documentno;
	}

	public String getClaveacceso() {
		return claveacceso;
	}

	public void setClaveacceso(String claveacceso) {
		this.claveacceso = claveacceso;
	}

	public String getAdClientId() {
		return adClientId;
	}

	public void setAdClientId(String adClientId) {
		this.adClientId = adClientId;
	}
}
