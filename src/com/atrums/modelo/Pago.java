package com.atrums.modelo;

import org.apache.log4j.Logger;

public class Pago {
	static final Logger log = Logger.getLogger(Pago.class);
	
	private String finPaymentmethodId;
	private String finPaymentScheduleId;
	private String finPaymentId;
	private String finPaymentDetailId;
	private String amount;
	private String referencia;
	
	/**
	 * Campos adicionales
	 * **/
	
	private String idCupon;
	private String tipoPago;
	private String idEmpresa;
	
	public Pago () {
		super();
	}

	public String getIdCupon() {
		return idCupon;
	}

	public void setIdCupon(String idCupon) {
		this.idCupon = idCupon;
	}

	public String getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

	public String getIdEmpresa() {
		return idEmpresa;
	}

	public void setIdEmpresa(String idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getFinPaymentDetailId() {
		return finPaymentDetailId;
	}

	public void setFinPaymentDetailId(String finPaymentDetailId) {
		this.finPaymentDetailId = finPaymentDetailId;
	}

	public String getFinPaymentId() {
		return finPaymentId;
	}

	public void setFinPaymentId(String finPaymentId) {
		this.finPaymentId = finPaymentId;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getFinPaymentScheduleId() {
		return finPaymentScheduleId;
	}

	public void setFinPaymentScheduleId(String finPaymentScheduleId) {
		this.finPaymentScheduleId = finPaymentScheduleId;
	}

	public String getFinPaymentmethodId() {
		return finPaymentmethodId;
	}

	public void setFinPaymentmethodId(String finPaymentmethodId) {
		this.finPaymentmethodId = finPaymentmethodId;
	}
}
