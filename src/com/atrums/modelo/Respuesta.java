package com.atrums.modelo;

import org.apache.log4j.Logger;

import com.atrums.dao.Operaciones;

public class Respuesta {
	static final Logger log = Logger.getLogger(Operaciones.class);
	
	private String empresa;
	private String nrodocumento;
	private String sucursal;
	private String ptoemision;
	private String tipodocumento;
	private String fecha;
	private String claveacceso;
	private String ruccliente;
	private String mensaje;
	private String mensajeerror;
	private boolean guardado;
	
	public Respuesta() {
		super();
	}

	public String getRuccliente() {
		return ruccliente;
	}

	public void setRuccliente(String ruccliente) {
		this.ruccliente = ruccliente;
	}

	public String getSucursal() {
		return sucursal;
	}

	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	public String getPtoemision() {
		return ptoemision;
	}

	public void setPtoemision(String ptoemision) {
		this.ptoemision = ptoemision;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getNrodocumento() {
		return nrodocumento;
	}

	public void setNrodocumento(String nrodocumento) {
		this.nrodocumento = nrodocumento;
	}

	public String getTipodocumento() {
		return tipodocumento;
	}

	public void setTipodocumento(String tipodocumento) {
		this.tipodocumento = tipodocumento;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getClaveacceso() {
		return claveacceso;
	}

	public void setClaveacceso(String claveacceso) {
		this.claveacceso = claveacceso;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getMensajeerror() {
		return mensajeerror;
	}

	public void setMensajeerror(String mensajeerror) {
		this.mensajeerror = mensajeerror;
	}

	public boolean isGuardado() {
		return guardado;
	}

	public void setGuardado(boolean guardado) {
		this.guardado = guardado;
	}
}
