package com.atrums.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;

import com.atrums.modelo.Respuesta;
import com.atrums.modelo.Configuracion;
import com.atrums.modelo.DocumentoLinea;
import com.atrums.modelo.Invoice;
import com.atrums.modelo.InvoiceLine;
import com.atrums.modelo.Pago;
import com.atrums.persistencia.ConexionPostgres;
import com.atrums.persistencia.OperacionesBDDPostgres;

@Path("/")
public class Operaciones {
	static final Logger log = Logger.getLogger(Operaciones.class);
	private DataSource dataSourceOpenbravo = ConexionPostgres.getDataSource();
	private Configuracion configuracion = new Configuracion();
	
	@GET
	@Path("/addDocumento")
	@Produces(MediaType.APPLICATION_JSON)
	public Respuesta addDocumento(@QueryParam("fa") String fa) {
		Respuesta auxRespuesta = new Respuesta();
		
		try {
			JSONObject auxInputJsonObj = new JSONObject(fa);
			auxRespuesta = this.addInvoice(auxInputJsonObj);
			log.warn(auxRespuesta.getMensajeerror());
		} catch (JSONException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			auxRespuesta.setMensajeerror("Error en JSON");
			auxRespuesta.setGuardado(false);
		}	
		
		return auxRespuesta;
	}
	
	@POST
	@Path("/addDocumento")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Respuesta addDocumentoPost(String inputJsonObj) {
		Respuesta auxRespuesta = new Respuesta();
		
		try {
			JSONObject auxInputJsonObj = new JSONObject(inputJsonObj);
			auxRespuesta = this.addInvoice(auxInputJsonObj);
			log.warn(auxRespuesta.getMensajeerror());
		} catch (JSONException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			auxRespuesta.setMensajeerror("Error en JSON");
			auxRespuesta.setGuardado(false);
		}	
		
		return auxRespuesta;
	}
	
	private Respuesta addInvoice(JSONObject dato) {
		Respuesta auxRespuesta = new Respuesta();
		auxRespuesta.setMensaje("FAIL");
		
		Invoice auxInvoice = new Invoice();
		List<InvoiceLine> auxInvoiceLineas = new ArrayList<InvoiceLine>();
		
		OperacionesBDDPostgres bddPostgres = new OperacionesBDDPostgres();
		Connection conPostgres = null;
		
		conPostgres = bddPostgres.getConneccion(this.dataSourceOpenbravo);		
		
		if (conPostgres != null) {
			
			/**
			 * Verificaci�n de la empresa en la bdd de Openbravo
			 * Fecha: 27/11/2017
			 * **/
			
			try {
				auxRespuesta.setEmpresa(dato.get("EMPRESA1").toString());
				
				List<String> auxAdClientId = bddPostgres.getAdClientId(auxRespuesta.getEmpresa(), conPostgres);
				
				if (auxAdClientId.isEmpty()) {
					auxRespuesta.setMensajeerror("No hay la empresa en la BDD");
					auxRespuesta.setGuardado(false);
					auxInvoice = null;
					auxAdClientId = null;
					try {conPostgres.close();} catch (SQLException e) {}
					
					return auxRespuesta;
				} else if (auxAdClientId.size() != 1) {
					auxRespuesta.setMensajeerror("Hay m�s de una empresa con el mismo nombre en la BDD");
					auxRespuesta.setGuardado(false);
					auxInvoice = null;
					auxAdClientId = null;
					try {conPostgres.close();} catch (SQLException e) {}
					
					return auxRespuesta;
				} else {
					auxInvoice.setAdClientId(auxAdClientId.get(0));
					auxAdClientId = null;
				}
				
			} catch (JSONException ex) {
				// TODO Auto-generated catch block
				log.warn(ex.getMessage());
				auxRespuesta.setMensajeerror("No hay la empresa en el JSON");
				auxRespuesta.setGuardado(false);
				auxInvoice = null;
				try {conPostgres.close();} catch (SQLException e) {}
				
				return auxRespuesta;
			}
			
			/**
			 * Verificando clave de acceso, organizaci�, y tipo de documento
			 * Fecha: 27/11/2017
			 * **/
			
			try {
				auxRespuesta.setClaveacceso(dato.get("Clave_acceso").toString());
				auxInvoice.setClaveacceso(auxRespuesta.getClaveacceso());
				
				String auxSQL = "SELECT count(ci.*) AS total "
						+ "FROM c_invoice ci "
						+ "WHERE ci.em_atecfe_codigo_acc = '" + auxInvoice.getClaveacceso() + "' AND ci.isactive = 'Y';";
				
				if (bddPostgres.isExite(auxSQL, conPostgres)) {
					auxRespuesta.setMensajeerror("Ya existe un documento con la clave de acceso en la BDD");
					auxRespuesta.setGuardado(false);
					auxInvoice = null;
					try {conPostgres.close();} catch (SQLException e) {}
					
					return auxRespuesta;
				}
				
				auxRespuesta.setNrodocumento(auxInvoice.getClaveacceso().substring(30, 39));
				auxInvoice.setDocumentno(auxRespuesta.getNrodocumento());
				
				auxRespuesta.setSucursal(auxInvoice.getClaveacceso().substring(24, 27));
				auxInvoice.setSucursal(auxRespuesta.getSucursal());
				
				auxRespuesta.setPtoemision(auxInvoice.getClaveacceso().substring(27, 30));
				auxInvoice.setPtoemision(auxRespuesta.getPtoemision());
				
				bddPostgres.setAuxInvoice(auxInvoice);
				
				List<String> auxAdOrgId = bddPostgres.getAdOrgId(conPostgres);
				
				if (auxAdOrgId.isEmpty()) {
					auxRespuesta.setMensajeerror("No hay la sucursal en la BDD");
					auxRespuesta.setGuardado(false);
					auxInvoice = null;
					auxAdOrgId = null;
					try {conPostgres.close();} catch (SQLException e) {}
					
					return auxRespuesta;
				} else if (auxAdOrgId.size() != 1) {
					auxRespuesta.setMensajeerror("Hay m�s de una sucursal con el mismo establecimiento y pto. de emisi�n en la BDD");
					auxRespuesta.setGuardado(false);
					auxInvoice = null;
					auxAdOrgId = null;
					try {conPostgres.close();} catch (SQLException e) {}
					
					return auxRespuesta;
				} else {
					auxInvoice.setAdOrgId(auxAdOrgId.get(0));
					auxAdOrgId = null;
				}
				
				auxRespuesta.setTipodocumento(auxInvoice.getClaveacceso().substring(8, 10));
				
				String auxDocumento = null;
				
				if (dato.get("Tipo_comprobante").toString().equals("10")) {
					auxDocumento = "TICKET DE VENTA";
				} else {
					auxDocumento = (auxRespuesta.getTipodocumento().equals("01"))? "FACTURA" : "NOTA";
				}
				
				bddPostgres.setAuxInvoice(auxInvoice);
				
				List<String> auxCDoctypeId = bddPostgres.getCDoctypeId(auxDocumento, conPostgres);
				
				if (auxCDoctypeId.isEmpty()) {
					auxRespuesta.setMensajeerror("No hay el tipo de documento " + auxDocumento + " en la sucursal " + auxInvoice.getSucursal() + " de la BDD");
					auxRespuesta.setGuardado(false);
					auxInvoice = null;
					auxCDoctypeId = null;
					try {conPostgres.close();} catch (SQLException e) {}
					
					return auxRespuesta;
				} else if (auxCDoctypeId.size() != 1) {
					auxRespuesta.setMensajeerror("Hay m�s de un tipo de documento " + auxDocumento + " en la sucursal " + auxInvoice.getSucursal() + " de la BDD");
					auxRespuesta.setGuardado(false);
					auxInvoice = null;
					auxCDoctypeId = null;
					try {conPostgres.close();} catch (SQLException e) {}
					
					return auxRespuesta;
				} else {
					auxInvoice.setcDoctypeId(auxCDoctypeId.get(0));
					auxCDoctypeId = null;
				}
				
				DateFormat formatori = new SimpleDateFormat("ddMMyyyy");
				DateFormat formatmod = new SimpleDateFormat("dd/MM/yyyy");
				
				Date auxDate = formatori.parse(auxInvoice.getClaveacceso().substring(0, 8));
				
				auxRespuesta.setFecha(formatmod.format(auxDate));
				auxInvoice.setDateinvoiced(auxRespuesta.getFecha());
				
			} catch (JSONException ex) {
				// TODO Auto-generated catch block
				log.warn(ex.getMessage());
				auxRespuesta.setMensajeerror("No hay la clave de acceso en el JSON");
				auxRespuesta.setGuardado(false);
				auxInvoice = null;
				try {conPostgres.close();} catch (SQLException e) {}
				
				return auxRespuesta;
			} catch (StringIndexOutOfBoundsException ex) {
				// TODO: handle exception
				log.warn(ex.getMessage());
				auxRespuesta.setMensajeerror("Clave de Acceso mal formada");
				auxRespuesta.setGuardado(false);
				auxInvoice = null;
				try {conPostgres.close();} catch (SQLException e) {}
				
				return auxRespuesta;
			} catch (ParseException ex) {
				// TODO: handle exception
				log.warn(ex.getMessage());
				auxRespuesta.setMensajeerror("Clave de Acceso mal formada");
				auxRespuesta.setGuardado(false);
				auxInvoice = null;
				try {conPostgres.close();} catch (SQLException e) {}
				
				return auxRespuesta;
			}
						
			/**
			 * Verificaci�n del cliente en la bdd de Openbravo
			 * Fecha: 27/11/2017
			 * **/
			
			try {
				auxRespuesta.setRuccliente(dato.get("Ruc_cliente").toString());
				
				if (auxRespuesta.getRuccliente().equals("9999999999")) {
					auxRespuesta.setRuccliente("9999999999999");
				}
				
				List<String> auxCBpartnerId = bddPostgres.getCBpartnerId(auxRespuesta.getRuccliente(), conPostgres);
				
				if (auxCBpartnerId.isEmpty()) {
					String auxTipoRucCi = null;
					
					if (auxRespuesta.getRuccliente().equals("9999999999999")) {
						auxTipoRucCi = "07";
					} else if (auxRespuesta.getRuccliente().length() == 10) {
						auxTipoRucCi = "02";
					} else if (auxRespuesta.getRuccliente().length() == 13) {
						auxTipoRucCi = "01";
					} else {
						auxTipoRucCi = "03";
					}
					
					String auxNombreCliente = dato.get("Nombre_cliente").toString();
					String auxEmailCliente = dato.get("Correo_cliente").toString();
					
					auxInvoice.setcBpartnerId(bddPostgres.putCBpartnerId(auxNombreCliente, auxEmailCliente, auxRespuesta.getRuccliente(), auxTipoRucCi, conPostgres));
					
					if (auxInvoice.getcBpartnerId() == null) {
						auxRespuesta.setMensajeerror("No se guardo el cliente en la BDD, intentelo de nuevo");
						auxRespuesta.setGuardado(false);
						auxInvoice = null;
						auxCBpartnerId = null;
						try {conPostgres.rollback();} catch (SQLException e1) {}
						try {conPostgres.close();} catch (SQLException e) {}
						
						return auxRespuesta;
					}
					
					auxInvoice.setAdUserId(bddPostgres.putAdUserId(auxNombreCliente, auxEmailCliente, "", conPostgres));
					
					if (auxInvoice.getAdUserId() == null) {
						auxRespuesta.setMensajeerror("No se guardo el cliente en la BDD, intentelo de nuevo");
						auxRespuesta.setGuardado(false);
						auxInvoice = null;
						auxCBpartnerId = null;
						try {conPostgres.rollback();} catch (SQLException e1) {}
						try {conPostgres.close();} catch (SQLException e) {}
						
						return auxRespuesta;
					}
					
					String auxDireccionCliente = dato.get("Dirrecion").toString();
					
					auxInvoice.setcLocationId(bddPostgres.putCLocationId(auxDireccionCliente, conPostgres));
					
					if (auxInvoice.getcLocationId() == null) {
						auxRespuesta.setMensajeerror("No se guardo la direcci�n del cliente en la BDD, intentelo de nuevo");
						auxRespuesta.setGuardado(false);
						auxInvoice = null;
						auxCBpartnerId = null;
						try {conPostgres.rollback();} catch (SQLException e1) {}
						try {conPostgres.close();} catch (SQLException e) {}
						
						return auxRespuesta;
					}
					
					bddPostgres.setAuxInvoice(auxInvoice);
					
					auxInvoice.setcBpartnerLocationId(bddPostgres.putCBpartnerLocationId(auxDireccionCliente, conPostgres));
					
					if (auxInvoice.getcBpartnerLocationId() == null) {
						auxRespuesta.setMensajeerror("No se guardo la direccion del cliente en la BDD, intentelo de nuevo");
						auxRespuesta.setGuardado(false);
						auxInvoice = null;
						auxCBpartnerId = null;
						try {conPostgres.rollback();} catch (SQLException e1) {}
						try {conPostgres.close();} catch (SQLException e) {}
						
						return auxRespuesta;
					}
					
					bddPostgres.setAuxInvoice(auxInvoice);
					
				} else if (auxCBpartnerId.size() != 1) {
					auxRespuesta.setMensajeerror("Hay m�s de un clinte con los mismos datos en la BDD");
					auxRespuesta.setGuardado(false);
					auxInvoice = null;
					auxCBpartnerId = null;
					try {conPostgres.close();} catch (SQLException e) {}
					
					return auxRespuesta;
				} else {
					auxInvoice.setcBpartnerId(auxCBpartnerId.get(0));
					auxCBpartnerId = null;
					
					auxInvoice.setcBpartnerLocationId(bddPostgres.getCBpartnerLocationId(conPostgres));
					
					if (auxInvoice.getcBpartnerLocationId() == null) {
						String auxDireccionCliente = dato.get("Dirrecion").toString();
						
						auxInvoice.setcLocationId(bddPostgres.putCLocationId(auxDireccionCliente, conPostgres));
						
						if (auxInvoice.getcLocationId() == null) {
							auxRespuesta.setMensajeerror("No se guardo la direccion del cliente en la BDD, intentelo de nuevo");
							auxRespuesta.setGuardado(false);
							auxInvoice = null;
							auxCBpartnerId = null;
							try {conPostgres.rollback();} catch (SQLException e1) {}
							try {conPostgres.close();} catch (SQLException e) {}
							
							return auxRespuesta;
						}
						
						bddPostgres.setAuxInvoice(auxInvoice);
						
						auxInvoice.setcBpartnerLocationId(bddPostgres.putCBpartnerLocationId(auxDireccionCliente, conPostgres));
						
						if (auxInvoice.getcBpartnerLocationId() == null) {
							auxRespuesta.setMensajeerror("No se guardo la direccion del cliente en la BDD, intentelo de nuevo");
							auxRespuesta.setGuardado(false);
							auxInvoice = null;
							auxCBpartnerId = null;
							try {conPostgres.rollback();} catch (SQLException e1) {}
							try {conPostgres.close();} catch (SQLException e) {}
							
							return auxRespuesta;
						}
						
						bddPostgres.setAuxInvoice(auxInvoice);
					}
				}
				
			} catch (JSONException ex) {
				// TODO Auto-generated catch block
				log.warn(ex.getMessage());
				auxRespuesta.setMensajeerror("No hay los datos completos del cliente en el JSON");
				auxRespuesta.setGuardado(false);
				auxInvoice = null;
				try {conPostgres.close();} catch (SQLException e) {}
				
				return auxRespuesta;
			}
			
			/**
			 * Verificaci�n del check
			 * Fecha: 27/11/2017
			 * **/
			
			try {
				auxInvoice.setDescription("Check: " + dato.get("Check").toString());
				
				bddPostgres.setAuxInvoice(auxInvoice);
			} catch (JSONException ex) {
				// TODO Auto-generated catch block
				log.warn(ex.getMessage());
				auxRespuesta.setMensajeerror("No hay el Check en el JSON");
				auxRespuesta.setGuardado(false);
				auxInvoice = null;
				try {conPostgres.close();} catch (SQLException e) {}
				
				return auxRespuesta;
			}
			
			/**
			 * Verificaci�n de la autorizacion
			 * Fecha: 27/11/2017
			 * **/
			
			try {
				auxInvoice.setAutorization(dato.get("Autorizacion").toString());
				
				if (auxInvoice.getAutorization().equals("null") || auxInvoice.getAutorization().equals("")) {
					auxInvoice.setAutorization(auxInvoice.getClaveacceso());
				}
				
				bddPostgres.setAuxInvoice(auxInvoice);
			} catch (JSONException ex) {
				// TODO Auto-generated catch block
				log.warn(ex.getMessage());
				auxRespuesta.setMensajeerror("No hay la autorizaci�n en el JSON");
				auxRespuesta.setGuardado(false);
				auxInvoice = null;
				try {conPostgres.close();} catch (SQLException e) {}
				
				return auxRespuesta;
			}
			
			/**
			 * Verificaci�n del total del documento
			 * Fecha: 27/11/2017
			 * **/
			
			try {
				auxInvoice.setTotal(dato.get("Total").toString());
				
				bddPostgres.setAuxInvoice(auxInvoice);
			} catch (JSONException ex) {
				// TODO Auto-generated catch block
				log.warn(ex.getMessage());
				auxRespuesta.setMensajeerror("No hay el total del documento en el JSON");
				auxRespuesta.setGuardado(false);
				auxInvoice = null;
				try {conPostgres.close();} catch (SQLException e) {}
				
				return auxRespuesta;
			}
			
			/**
			 * Verificaci�n del Servicio
			 * Fecha: 27/11/2017
			 * **/
			
			try {
				auxInvoice.setServicio(Double.valueOf(dato.get("Servicio").toString()));
				
				bddPostgres.setAuxInvoice(auxInvoice);
			} catch (JSONException ex) {
				// TODO Auto-generated catch block
				auxInvoice.setServicio(Double.valueOf("0"));
			}
			
			/**
			 * Guardando Documento Cabecera
			 * Fecha: 28/11/2017
			 * **/
			
			auxInvoice.setcInvoiceId(bddPostgres.putCInvoiceId(conPostgres));
			
			if (auxInvoice.getcInvoiceId() == null) {
				auxRespuesta.setMensajeerror("No se guardo el documento en la BDD, intentelo de nuevo");
				auxRespuesta.setGuardado(false);
				auxInvoice = null;
				try {conPostgres.rollback();} catch (SQLException e1) {}
				try {conPostgres.close();} catch (SQLException e) {}
				
				return auxRespuesta;
			}
			
			/**
			 * Verificaci�n Lineas
			 * Fecha: 27/11/2017
			 * **/
			
			try {
				
				int linea = 0;
				
				for (int i=0; i<dato.getJSONArray("Items").length(); i++) {
					JSONObject auxLineaJson = (JSONObject) dato.getJSONArray("Items").get(i);
					
					InvoiceLine auxInvoiceLine = new InvoiceLine();
					
					linea = linea + 10;
					auxInvoiceLine.setLinea(linea);
					
					try {
						List<String> auxMProductId = bddPostgres.getMProductId(auxLineaJson.get("Codigo").toString(), conPostgres);
						
						if (auxMProductId.isEmpty()) {
							List<String> auxProductCatergoryId = bddPostgres.getMProductCategoryId(this.configuracion.getCategoriaproducto(), conPostgres); 
							
							if (auxProductCatergoryId.isEmpty()) {
								auxInvoiceLine.setmProductCategoryId(bddPostgres.putMProductCategoryId(this.configuracion.getCategoriaproducto(), conPostgres));
								
								if (auxInvoiceLine.getmProductCategoryId() == null) {
									auxRespuesta.setMensajeerror("No se guardo la categoria del producto c�digo " + this.configuracion.getCategoriaproducto() + " en la BDD");
									auxRespuesta.setGuardado(false);
									auxInvoice = null;
									auxMProductId = null;
									try {conPostgres.rollback();} catch (SQLException e1) {}
									try {conPostgres.close();} catch (SQLException e) {}
									
									return auxRespuesta;
								}
							} else if (auxProductCatergoryId.size() != 1) {
								auxRespuesta.setMensajeerror("Hay m�s de una categor�a del producto " + configuracion.getCategoriaproducto() + " en la BDD");
								auxRespuesta.setGuardado(false);
								auxInvoice = null;
								auxMProductId = null;
								try {conPostgres.rollback();} catch (SQLException e1) {}
								try {conPostgres.close();} catch (SQLException e) {}
								
								return auxRespuesta;
							} else {
								auxInvoiceLine.setmProductCategoryId(auxProductCatergoryId.get(0));
								auxProductCatergoryId = null;
							}
							
							DocumentoLinea auxLinea = new DocumentoLinea();
							auxLinea.setItemcdgodtfc(auxLineaJson.get("Codigo").toString());
							auxLinea.setDtfcdscr(auxLineaJson.get("Nombre").toString().replaceAll("'",""));
							
							auxInvoiceLine.setmProductId(bddPostgres.putMProductId(auxInvoiceLine.getmProductCategoryId(), auxLinea, conPostgres));
							
							if (auxInvoiceLine.getmProductId() == null) {
								auxRespuesta.setMensajeerror("No se guardo el producto con este c�digo " + auxLineaJson.get("Codigo") + " en la BDD");
								auxRespuesta.setGuardado(false);
								auxInvoice = null;
								auxMProductId = null;
								try {conPostgres.rollback();} catch (SQLException e1) {}
								try {conPostgres.close();} catch (SQLException e) {}
								
								return auxRespuesta;
							}
						} else if (auxMProductId.size() != 1) {
							auxRespuesta.setMensajeerror("Hay m�s de un producto con el mismo c�digo " + auxLineaJson.get("Codigo") + " en la BDD");
							auxRespuesta.setGuardado(false);
							auxInvoice = null;
							auxMProductId = null;
							try {conPostgres.rollback();} catch (SQLException e1) {}
							try {conPostgres.close();} catch (SQLException e) {}
							
							return auxRespuesta;
						} else {
							auxInvoiceLine.setmProductId(auxMProductId.get(0));
							auxMProductId = null;
						}
					} catch (JSONException ex) {
						// TODO Auto-generated catch block
						log.warn(ex.getMessage());
						auxRespuesta.setMensajeerror("No hay codigo del producto en el JSON");
						auxRespuesta.setGuardado(false);
						auxInvoice = null;
						try {conPostgres.rollback();} catch (SQLException e1) {}
						try {conPostgres.close();} catch (SQLException e) {}
						
						return auxRespuesta;
					}
					
					
					try {
						auxInvoiceLine.setCantidad(auxLineaJson.get("Cantidad").toString());
					} catch (JSONException ex) {
						// TODO Auto-generated catch block
						log.warn(ex.getMessage());
						auxRespuesta.setMensajeerror("No hay la cantidad en la linea del producto c�digo " + auxLineaJson.get("Codigo") + " en el JSON");
						auxRespuesta.setGuardado(false);
						auxInvoice = null;
						try {conPostgres.rollback();} catch (SQLException e1) {}
						try {conPostgres.close();} catch (SQLException e) {}
						
						return auxRespuesta;
					}
					
					try {
						auxInvoiceLine.setPrecioUnitario(auxLineaJson.get("Precio_unitario").toString());
					} catch (JSONException ex) {
						// TODO Auto-generated catch block
						log.warn(ex.getMessage());
						auxRespuesta.setMensajeerror("No hay precio unitario en la linea del producto c�digo " + auxLineaJson.get("Codigo") + " en el JSON");
						auxRespuesta.setGuardado(false);
						auxInvoice = null;
						try {conPostgres.rollback();} catch (SQLException e1) {}
						try {conPostgres.close();} catch (SQLException e) {}
						
						return auxRespuesta;
					}
					
					try {
						auxInvoiceLine.setDescuento(auxLineaJson.get("Descuento_linea").toString());
					} catch (JSONException ex) {
						// TODO Auto-generated catch block
						auxInvoiceLine.setDescuento("0");
					}
					
					try {
						auxInvoiceLine.setServicio(auxLineaJson.get("Servicio").toString());
					} catch (JSONException ex) {
						// TODO Auto-generated catch block
						auxInvoiceLine.setServicio("0");
					}
					
					DecimalFormat format2 = new DecimalFormat("0.00");
					double auxPorcentajeDescuento = 0;
					
					if (Double.valueOf(auxInvoiceLine.getPrecioUnitario()) != 0) {
						auxPorcentajeDescuento = Double.valueOf(auxInvoiceLine.getDescuento()) * (100/Double.valueOf(auxInvoiceLine.getPrecioUnitario()));
					} 
					
					auxInvoiceLine.setPorcentajeDescuento(format2.format(auxPorcentajeDescuento).replace(",", "."));
					
					try {
						auxInvoiceLine.setTotalLinea(auxLineaJson.get("Total").toString());
					} catch (JSONException ex) {
						// TODO Auto-generated catch block
						log.warn(ex.getMessage());
						auxRespuesta.setMensajeerror("No hay total de la linea del producto c�digo " + auxLineaJson.get("Codigo") + " en el JSON");
						auxRespuesta.setGuardado(false);
						auxInvoice = null;
						try {conPostgres.rollback();} catch (SQLException e1) {}
						try {conPostgres.close();} catch (SQLException e) {}
						
						return auxRespuesta;
					}
					
					try {
						auxInvoiceLine.setValor(auxLineaJson.get("Impuesto_linea").toString());
						
						double auxServicio = Double.valueOf(auxInvoiceLine.getServicio());
						double auxImpuesto = Double.valueOf(auxInvoiceLine.getValor());
						double auxTotal = Double.valueOf(auxInvoiceLine.getTotalLinea());
						
						auxInvoiceLine.setTotalLineaSinImpuestos(format2.format(auxTotal - auxImpuesto - auxServicio).replace(",", "."));
					} catch (JSONException ex) {
						// TODO Auto-generated catch block
						log.warn(ex.getMessage());
						auxRespuesta.setMensajeerror("No hay impuesto de la linea del producto c�digo " + auxLineaJson.get("Codigo") + " en el JSON");
						auxRespuesta.setGuardado(false);
						auxInvoice = null;
						try {conPostgres.rollback();} catch (SQLException e1) {}
						try {conPostgres.close();} catch (SQLException e) {}
						
						return auxRespuesta;
					}
					
					try {
						if (auxInvoice.getServicio() > 0) {
							if (auxLineaJson.get("Tipo_impuesto").toString().equals("0")) {
								auxInvoiceLine.setPorcentaje("SERVICIO+IVA0%");
							}else if (auxLineaJson.get("Tipo_impuesto").toString().equals("2")) {
								auxInvoiceLine.setPorcentaje("SERVICIO+IVA12%");
							}else if (auxLineaJson.get("Tipo_impuesto").toString().equals("3")) {
								auxInvoiceLine.setPorcentaje("SERVICIO+IVA14%");
							}else if (auxLineaJson.get("Tipo_impuesto").toString().equals("7") || auxLineaJson.get("Tipo_impuesto").toString().equals("6")) {
								auxInvoiceLine.setPorcentaje("SERVICIO+EXCENTO");
							}
						}else if (auxLineaJson.get("Tipo_impuesto").toString().equals("0")) {
							auxInvoiceLine.setPorcentaje("IVA 0%");
						}else if (auxLineaJson.get("Tipo_impuesto").toString().equals("2")) {
							auxInvoiceLine.setPorcentaje("IVA 12%");
						}else if (auxLineaJson.get("Tipo_impuesto").toString().equals("3")) {
							auxInvoiceLine.setPorcentaje("IVA 14%");
						}else if (auxLineaJson.get("Tipo_impuesto").toString().equals("7") || auxLineaJson.get("Tipo_impuesto").toString().equals("6")) {
							auxInvoiceLine.setPorcentaje("EXCENTO");
						}
						
						List<String> auxCTaxId = bddPostgres.getCTaxId(auxInvoiceLine.getPorcentaje(), conPostgres);
						
						if (auxCTaxId.isEmpty()) {
							auxRespuesta.setMensajeerror("No hay el impuesto " + auxInvoiceLine.getPorcentaje() + " en la BDD");
							auxRespuesta.setGuardado(false);
							auxInvoice = null;
							auxCTaxId = null;
							try {conPostgres.rollback();} catch (SQLException e1) {}
							try {conPostgres.close();} catch (SQLException e) {}
							
							return auxRespuesta;
						} else if (auxCTaxId.size() != 1) {
							auxRespuesta.setMensajeerror("Hay m�s de un impuesto " + auxInvoiceLine.getPorcentaje() + " en la BDD");
							auxRespuesta.setGuardado(false);
							auxInvoice = null;
							auxCTaxId = null;
							try {conPostgres.rollback();} catch (SQLException e1) {}
							try {conPostgres.close();} catch (SQLException e) {}
							
							return auxRespuesta;
						} else {
							auxInvoiceLine.setcTaxId(auxCTaxId.get(0));
							auxCTaxId = null;
						}
					} catch (JSONException ex) {
						// TODO Auto-generated catch block
						log.warn(ex.getMessage());
						auxRespuesta.setMensajeerror("No hay el tipo de impuesto en la linea del producto c�digo " + auxLineaJson.get("Codigo") + " en el JSON");
						auxRespuesta.setGuardado(false);
						auxInvoice = null;
						try {conPostgres.rollback();} catch (SQLException e1) {}
						try {conPostgres.close();} catch (SQLException e) {}
						
						return auxRespuesta;
					}
					
					auxInvoiceLine.setcInvoiceLineId(bddPostgres.putCInvoiceLineId(auxInvoiceLine, conPostgres));
					
					if (auxInvoiceLine.getcInvoiceLineId() == null) {
						auxRespuesta.setMensajeerror("No se guardo la linea del producto c�digo " + auxLineaJson.get("Codigo") + " en la BDD, intentelo de nuevo");
						auxRespuesta.setGuardado(false);
						auxInvoice = null;
						auxInvoiceLineas = null;
						
						try {conPostgres.rollback();} catch (SQLException e1) {}
						try {conPostgres.close();} catch (SQLException e) {}
						
						return auxRespuesta;
					}
					
					auxInvoiceLineas.add(auxInvoiceLine);
				}
				
				/**
				 * Procesando el descuento como item
				 * **/
				
				double auxDescuento = Double.valueOf(dato.get("Descuento").toString());
				
				if (auxDescuento > 0) {
					InvoiceLine auxInvoiceLine = new InvoiceLine();
					
					linea = linea + 10;
					auxInvoiceLine.setLinea(linea);
					
					List<String> auxMProductId = bddPostgres.getMProductId("Descuento".toUpperCase(), conPostgres);
					
					if (auxMProductId.isEmpty()) {
						auxRespuesta.setMensajeerror("No hay un producto con este c�digo Descuento en la BDD");
						auxRespuesta.setGuardado(false);
						auxInvoice = null;
						auxMProductId = null;
						try {conPostgres.rollback();} catch (SQLException e1) {}
						try {conPostgres.close();} catch (SQLException e) {}
						
						return auxRespuesta;
					} else if (auxMProductId.size() != 1) {
						auxRespuesta.setMensajeerror("Hay m�s de un producto con el mismo c�digo Descuento en la BDD");
						auxRespuesta.setGuardado(false);
						auxInvoice = null;
						auxMProductId = null;
						try {conPostgres.rollback();} catch (SQLException e1) {}
						try {conPostgres.close();} catch (SQLException e) {}
						
						return auxRespuesta;
					} else {
						auxInvoiceLine.setmProductId(auxMProductId.get(0));
						auxMProductId = null;
					}
					
					auxInvoiceLine.setCantidad("1");
					
					try {
						auxInvoiceLine.setPrecioUnitario("-" + dato.get("Descuento").toString());
					} catch (JSONException ex) {
						// TODO Auto-generated catch block
						log.warn(ex.getMessage());
						auxRespuesta.setMensajeerror("No hay precio unitario en la linea del producto c�digo Descuento en el JSON");
						auxRespuesta.setGuardado(false);
						auxInvoice = null;
						try {conPostgres.rollback();} catch (SQLException e1) {}
						try {conPostgres.close();} catch (SQLException e) {}
						
						return auxRespuesta;
					}
					
					auxInvoiceLine.setDescuento("0");
					auxInvoiceLine.setServicio("0");
					
					DecimalFormat format2 = new DecimalFormat("0.00");
					double auxPorcentajeDescuento = 0;
					
					if (Double.valueOf(auxInvoiceLine.getPrecioUnitario()) != 0) {
						auxPorcentajeDescuento = Double.valueOf(auxInvoiceLine.getDescuento()) * (100/Double.valueOf(auxInvoiceLine.getPrecioUnitario()));
					} 
					
					auxInvoiceLine.setPorcentajeDescuento(format2.format(auxPorcentajeDescuento).replace(",", "."));
					
					try {
						auxInvoiceLine.setTotalLinea("-" + dato.get("Descuento").toString());
					} catch (JSONException ex) {
						// TODO Auto-generated catch block
						log.warn(ex.getMessage());
						auxRespuesta.setMensajeerror("No hay total de la linea del producto c�digo Descuento en el JSON");
						auxRespuesta.setGuardado(false);
						auxInvoice = null;
						try {conPostgres.rollback();} catch (SQLException e1) {}
						try {conPostgres.close();} catch (SQLException e) {}
						
						return auxRespuesta;
					}
					
					auxInvoiceLine.setValor("0");
					
					double auxServicio = Double.valueOf(auxInvoiceLine.getServicio());
					double auxImpuesto = Double.valueOf(auxInvoiceLine.getValor());
					double auxTotal = Double.valueOf(auxInvoiceLine.getTotalLinea());
					
					auxInvoiceLine.setTotalLineaSinImpuestos(format2.format(auxTotal - auxImpuesto - auxServicio).replace(",", "."));
					
					auxInvoiceLine.setPorcentaje("IVA 0%");
					
					List<String> auxCTaxId = bddPostgres.getCTaxId(auxInvoiceLine.getPorcentaje(), conPostgres);
					
					if (auxCTaxId.isEmpty()) {
						auxRespuesta.setMensajeerror("No hay el impuesto " + auxInvoiceLine.getPorcentaje() + " en la BDD");
						auxRespuesta.setGuardado(false);
						auxInvoice = null;
						auxCTaxId = null;
						try {conPostgres.rollback();} catch (SQLException e1) {}
						try {conPostgres.close();} catch (SQLException e) {}
						
						return auxRespuesta;
					} else if (auxCTaxId.size() != 1) {
						auxRespuesta.setMensajeerror("Hay m�s de un impuesto " + auxInvoiceLine.getPorcentaje() + " en la BDD");
						auxRespuesta.setGuardado(false);
						auxInvoice = null;
						auxCTaxId = null;
						try {conPostgres.rollback();} catch (SQLException e1) {}
						try {conPostgres.close();} catch (SQLException e) {}
						
						return auxRespuesta;
					} else {
						auxInvoiceLine.setcTaxId(auxCTaxId.get(0));
						auxCTaxId = null;
					}
					
					auxInvoiceLine.setcInvoiceLineId(bddPostgres.putCInvoiceLineId(auxInvoiceLine, conPostgres));
					
					if (auxInvoiceLine.getcInvoiceLineId() == null) {
						auxRespuesta.setMensajeerror("No se guardo la linea del producto c�digo Descuento en la BDD, intentelo de nuevo");
						auxRespuesta.setGuardado(false);
						auxInvoice = null;
						auxInvoiceLineas = null;
						
						try {conPostgres.rollback();} catch (SQLException e1) {}
						try {conPostgres.close();} catch (SQLException e) {}
						
						return auxRespuesta;
					}
					
					auxInvoiceLineas.add(auxInvoiceLine);
				}
				
				if (!bddPostgres.procesarDocumento(dato, auxInvoiceLineas, conPostgres)) {
					auxRespuesta.setMensajeerror("# documento: " + auxInvoice.getDocumentno() + ", " + bddPostgres.getMensajeError());
					auxRespuesta.setGuardado(false);
					auxInvoice = null;
					auxInvoiceLineas = null;
					try {conPostgres.rollback();} catch (SQLException e) {}
					try {conPostgres.close();} catch (SQLException e) {}
					
					return auxRespuesta;
				}
			} catch (JSONException ex) {
				// TODO Auto-generated catch block
				log.warn(ex.getMessage());
				auxRespuesta.setMensajeerror("No hay el descuento en el JSON");
				auxRespuesta.setGuardado(false);
				auxInvoice = null;
				try {conPostgres.close();} catch (SQLException e) {}
				
				return auxRespuesta;
			}
			
			/**
			 * Verificaci�n de pagos
			 * Fecha: 27/11/2017
			 * **/
			
			try {
				
				if (auxRespuesta.getTipodocumento().equals("01") && Double.valueOf(auxInvoice.getTotal()) != 0) {
					
					double totalPagos = 0;
					
					for (int i=0; i<dato.getJSONArray("Formaspago").length(); i++) {
						JSONObject auxPagoJson = (JSONObject) dato.getJSONArray("Formaspago").get(i);
						
						Pago pago = new Pago();
						
						List<String> auxFinPaymentmethodId = bddPostgres.getFinPaymentmethodId(auxPagoJson.getString("Tipo").toString(), conPostgres);
						
						if (auxFinPaymentmethodId.isEmpty()) {
							auxInvoice.setDescription(auxInvoice.getDescription() + ", Tiene un metodo de pago " + auxPagoJson.getString("Tipo").toString() +  " : " + auxPagoJson.getString("Monto").toString() + " que no hay en la bdd");
							auxFinPaymentmethodId = null;
						} else {
							pago.setFinPaymentmethodId(auxFinPaymentmethodId.get(0));
							
							List<String> auxFinPaymentScheduleId = bddPostgres.getFinPaymentScheduleId(conPostgres);
							
							if (auxFinPaymentScheduleId.isEmpty()) {
								auxRespuesta.setMensajeerror("No hay plan de pagos en el documento en la bdd, intentelo de nuevo");
								auxRespuesta.setGuardado(false);
								auxInvoice = null;
								auxInvoiceLineas = null;
								pago = null;
								
								try {conPostgres.rollback();} catch (SQLException e) {}
								try {conPostgres.close();} catch (SQLException e) {}
								
								return auxRespuesta;
							}else{
								pago.setFinPaymentScheduleId(auxFinPaymentScheduleId.get(0));
								pago.setAmount(auxPagoJson.getString("Monto").toString().replaceAll(",", "."));
								pago.setReferencia("M�todo: " + auxPagoJson.getString("Tipo").toString() + " - Referencia: " + auxPagoJson.getString("Referencia").toString() + " ");
								
								try {
									pago.setIdCupon(auxPagoJson.getString("Id_cupon").toString());
									pago.setTipoPago(auxPagoJson.getString("Tipo_pago").toString());
									pago.setIdEmpresa(auxPagoJson.getString("Id_empresa").toString());
								} catch (JSONException ex) {
									// TODO Auto-generated catch blocks
								}
								
								totalPagos = totalPagos + Double.valueOf(pago.getAmount());
								
								pago.setFinPaymentId(bddPostgres.putFinPaymentId(pago, conPostgres));
								
								if (pago.getFinPaymentId() == null) {
									auxRespuesta.setMensajeerror("No se ingreso el pago en la bdd, intentelo de nuevo");
									auxRespuesta.setGuardado(false);
									auxInvoice = null;
									auxInvoiceLineas = null;
									pago = null;
									
									try {conPostgres.rollback();} catch (SQLException e) {}
									try {conPostgres.close();} catch (SQLException e) {}
									
									return auxRespuesta;
								} else {
									pago.setFinPaymentDetailId(bddPostgres.putFinPaymentDetailId(pago, conPostgres));
									
									if (pago.getFinPaymentDetailId() == null) {
										auxRespuesta.setMensajeerror("No se ingreso el pago en la bdd, intentelo de nuevo");
										auxRespuesta.setGuardado(false);
										auxInvoice = null;
										auxInvoiceLineas = null;
										pago = null;
										
										try {conPostgres.rollback();} catch (SQLException e) {}
										try {conPostgres.close();} catch (SQLException e) {}
										
										return auxRespuesta;
									} else {
										if (!bddPostgres.updateFinPaymentScheduledetailId(pago, conPostgres)) {
											auxRespuesta.setMensajeerror("No se ingreso el pago en la bdd, intentelo de nuevo");
											auxRespuesta.setGuardado(false);
											auxInvoice = null;
											auxInvoiceLineas = null;
											pago = null;
											
											try {conPostgres.rollback();} catch (SQLException e) {}
											try {conPostgres.close();} catch (SQLException e) {}
											
											return auxRespuesta;
										} else {
											if (!bddPostgres.procesarPago(pago, conPostgres)) {
												auxRespuesta.setMensajeerror("No se ingreso el pago en la bdd, intentelo de nuevo");
												auxRespuesta.setGuardado(false);
												auxInvoice = null;
												auxInvoiceLineas = null;
												pago = null;
												
												try {conPostgres.rollback();} catch (SQLException e) {}
												try {conPostgres.close();} catch (SQLException e) {}
												
												return auxRespuesta;
											}
										}
									}
								}
								
								auxFinPaymentScheduleId = null;
							}
							
							auxFinPaymentmethodId = null;
						}
					}
					
					double auxtotal = Double.valueOf(auxInvoice.getTotal());
					boolean isPagado = auxtotal == totalPagos? true : false;
					bddPostgres.setAuxInvoice(auxInvoice);
					
					if (!bddPostgres.terminarFactura(isPagado, totalPagos, conPostgres)) {
						auxRespuesta.setMensajeerror("No se ingreso el pago en la bdd, intentelo de nuevo");
						auxRespuesta.setGuardado(false);
						auxInvoice = null;
						auxInvoiceLineas = null;
						
						try {conPostgres.rollback();} catch (SQLException e) {}
						try {conPostgres.close();} catch (SQLException e) {}
						
						return auxRespuesta;
					}
				}
			} catch (JSONException ex) {
				// TODO Auto-generated catch block
				log.warn(ex.getMessage());
				auxRespuesta.setMensajeerror("No hay pagos en el JSON");
				auxRespuesta.setGuardado(false);
				auxInvoice = null;
				try {conPostgres.close();} catch (SQLException e) {}
				
				return auxRespuesta;
			}
			
			/**
			 * Terminado conexion e ingreso
			 * **/
			auxRespuesta.setMensaje("SUCCESS");
			auxRespuesta.setMensajeerror(null);
			auxRespuesta.setGuardado(true);
			try {conPostgres.commit();} catch (SQLException e1) {}
			//try {conPostgres.rollback();} catch (SQLException e1) {}
			try {conPostgres.close();} catch (SQLException e) {}
			
		} else {
			auxRespuesta.setMensajeerror("Intente de nuevo no hay conexion con la BDD");
			auxRespuesta.setGuardado(false);
		}
		
		auxInvoice = null;
		
		return auxRespuesta;
	}
	
}
