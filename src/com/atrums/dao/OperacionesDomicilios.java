package com.atrums.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.atrums.modelo.Configuracion;
import com.atrums.modelo.Documento;
import com.atrums.modelo.DocumentoLinea;
import com.atrums.modelo.Invoice;
import com.atrums.modelo.InvoiceLine;
import com.atrums.persistencia.ConexionOracle;
import com.atrums.persistencia.ConexionPostgres;
import com.atrums.persistencia.OperacionesBDDOracle;
import com.atrums.persistencia.OperacionesBDDPostgres;

public class OperacionesDomicilios implements Runnable {
	static final Logger log = Logger.getLogger(OperacionesDomicilios.class);
	private DataSource dataSourceOracle = ConexionOracle.getDataSource();
	private DataSource dataSourcePostgres = ConexionPostgres.getDataSource();
	private Configuracion configuracion = new Configuracion();
	private boolean monitorearDomicilios = true;
	private boolean migrarDocumento = true;
	private boolean exception = false;
	
	public OperacionesDomicilios() {
		super();
	}
	
	public void run() {
		// TODO Auto-generated method stub
		if (this.configuracion.getEmpresa().equals("")) {
			this.monitorearDomicilios = false;
			log.info("No hay empresa configurada para la migraci�n de domicilios");
		}
		
		while (this.monitorearDomicilios) {
			try {
				//try {Thread.sleep(60000);} catch (InterruptedException ex) {}
				
				OperacionesBDDOracle bddOracle = new OperacionesBDDOracle();
				Connection conOracle = null;
				
				OperacionesBDDPostgres bddPostgres = new OperacionesBDDPostgres();
				Connection conPostgres = null;
				
				conOracle = bddOracle.getConneccion(this.dataSourceOracle);	
				conPostgres = bddPostgres.getConneccion(this.dataSourcePostgres);
				
				if (conOracle != null && conPostgres != null) {
					try {
						this.migrarDocumento = true;
						
						while (this.migrarDocumento) {
							bddPostgres.setException(false);
							bddOracle.setException(false);
							bddPostgres.setAuxInvoice(null);
							bddPostgres.setMensajeError(null);
							bddOracle.setMensajeError(null);
							
							List<Documento> documentos = bddOracle.getDocumento(conOracle);
							
							if (documentos.isEmpty()) {
								this.migrarDocumento = false;
							}
							
							if (this.migrarDocumento) {
								Invoice auxInvoice = new Invoice();
								List<InvoiceLine> auxInvoiceLineas = new ArrayList<InvoiceLine>();
								
								List<String> auxAdClientId = bddPostgres.getAdClientId(this.configuracion.getEmpresa(), conPostgres);
								
								if (auxAdClientId.isEmpty()) {
									log.warn("No hay la empresa en la BDD");
									this.migrarDocumento = false;
									conPostgres.rollback();
									break;
								} else if (auxAdClientId.size() != 1) {
									log.warn("Hay m�s de una empresa con el mismo nombre en la BDD");
									this.migrarDocumento = false;
									conPostgres.rollback();
									break;
								} else {
									auxInvoice.setAdClientId(auxAdClientId.get(0));
									auxAdClientId = null;
								}
								
								String auxClaveProvisional = documentos.get(0).getFctrnmro().replaceAll("-", "");
								
								auxInvoice.setSucursal(auxClaveProvisional.substring(0, 3));
								auxInvoice.setPtoemision(auxClaveProvisional.substring(3, 6));
								auxInvoice.setDocumentno(auxClaveProvisional.substring(6, 15));
								
								bddPostgres.setAuxInvoice(auxInvoice);
								
								List<String> auxAdOrgId = bddPostgres.getAdOrgId(conPostgres);
								
								if (auxAdOrgId.isEmpty()) {
									log.warn("No hay la sucursal en la BDD: " + auxInvoice.getSucursal() + " - " + auxInvoice.getPtoemision() + " - " + auxInvoice.getDocumentno());
									this.migrarDocumento = false;
									conPostgres.rollback();
									break;
								} else if (auxAdOrgId.size() != 1) {
									log.warn("Hay m�s de una sucursal con el mismo establecimiento y pto. de emisi�n en la BDD");
									this.migrarDocumento = false;
									conPostgres.rollback();
									break;
								} else {
									auxInvoice.setAdOrgId(auxAdOrgId.get(0));
									auxAdOrgId = null;
								}
								
								String auxDocumento = "FACTURA";
								
								bddPostgres.setAuxInvoice(auxInvoice);
								
								List<String> auxCDoctypeId = bddPostgres.getCDoctypeId(auxDocumento, conPostgres);
								
								if (auxCDoctypeId.isEmpty()) {
									log.warn("No hay el tipo de documento " + auxDocumento + " en la sucursal " + auxInvoice.getSucursal() + " de la BDD");
									this.migrarDocumento = false;
									conPostgres.rollback();
									break;
								} else if (auxCDoctypeId.size() != 1) {
									log.warn("Hay m�s de un tipo de documento " + auxDocumento + " en la sucursal " + auxInvoice.getSucursal() + " de la BDD");
									this.migrarDocumento = false;
									conPostgres.rollback();
									break;
								} else {
									auxInvoice.setcDoctypeId(auxCDoctypeId.get(0));
									auxCDoctypeId = null;
								}
								
								DateFormat formatori = new SimpleDateFormat("ddMMyyyy");
								DateFormat formatmod = new SimpleDateFormat("dd/MM/yyyy");
								
								Date auxDate = formatori.parse(documentos.get(0).getFctrfcha());
								
								auxInvoice.setDateinvoiced(formatmod.format(auxDate));
								
								String auxRucCliente = documentos.get(0).getClntcdgofctr();
								
								if (auxRucCliente.equals("9999999999")) {
									auxRucCliente = "9999999999999";
								}
								
								List<String> auxCBpartnerId = bddPostgres.getCBpartnerId(auxRucCliente, conPostgres);
								
								if (auxCBpartnerId.isEmpty()) {
									String auxTipoRucCi = null;
									
									if (auxRucCliente.equals("9999999999999")) {
										auxTipoRucCi = "07";
									} else if (auxRucCliente.length() == 10) {
										auxTipoRucCi = "02";
									} else if (auxRucCliente.length() == 13) {
										auxTipoRucCi = "01";
									} else {
										auxTipoRucCi = "03";
									}
									
									String auxNombreCliente = documentos.get(0).getFctrprsn();
									String auxEmailCliente = documentos.get(0).getFctrclntmail();
									
									auxInvoice.setcBpartnerId(bddPostgres.putCBpartnerId(auxNombreCliente, auxEmailCliente, auxRucCliente, auxTipoRucCi, conPostgres));
									
									if (auxInvoice.getcBpartnerId() == null) {
										log.warn("No se guardo el cliente en la BDD, intentelo de nuevo");
										this.migrarDocumento = false;
										conPostgres.rollback();
										break;
									}
									
									auxInvoice.setAdUserId(bddPostgres.putAdUserId(auxNombreCliente, auxEmailCliente, "", conPostgres));
									
									if (auxInvoice.getAdUserId() == null) {
										log.warn("No se guardo el cliente en la BDD, intentelo de nuevo");
										this.migrarDocumento = false;
										conPostgres.rollback();
										break;
									}
									
									String auxDireccionCliente = documentos.get(0).getFctrdire();
									
									auxInvoice.setcLocationId(bddPostgres.putCLocationId(auxDireccionCliente, conPostgres));
									
									if (auxInvoice.getcLocationId() == null) {
										log.warn("No se guardo la direccion del cliente en la BDD, intentelo de nuevo");
										this.migrarDocumento = false;
										conPostgres.rollback();
										break;
									}
									
									bddPostgres.setAuxInvoice(auxInvoice);
									
									auxInvoice.setcBpartnerLocationId(bddPostgres.putCBpartnerLocationId(auxDireccionCliente, conPostgres));
									
									if (auxInvoice.getcBpartnerLocationId() == null) {
										log.warn("No se guardo la direccion del cliente en la BDD, intentelo de nuevo");
										this.migrarDocumento = false;
										conPostgres.rollback();
										break;
									}
									
									bddPostgres.setAuxInvoice(auxInvoice);
								} else if (auxCBpartnerId.size() != 1) {
									log.warn("Hay m�s de un clinte con los mismos datos en la BDD");
									this.migrarDocumento = false;
									conPostgres.rollback();
									break;
								} else {
									auxInvoice.setcBpartnerId(auxCBpartnerId.get(0));
									auxCBpartnerId = null;
									
									auxInvoice.setcBpartnerLocationId(bddPostgres.getCBpartnerLocationId(conPostgres));
									
									if (auxInvoice.getcBpartnerLocationId() == null) {
										String auxDireccionCliente = documentos.get(0).getFctrdire();
										
										auxInvoice.setcLocationId(bddPostgres.putCLocationId(auxDireccionCliente, conPostgres));
										
										if (auxInvoice.getcLocationId() == null) {
											log.warn("No se guardo la direccion del cliente en la BDD, intentelo de nuevo");
											this.migrarDocumento = false;
											conPostgres.rollback();
											break;
										}
										
										bddPostgres.setAuxInvoice(auxInvoice);
										
										auxInvoice.setcBpartnerLocationId(bddPostgres.putCBpartnerLocationId(auxDireccionCliente, conPostgres));
										
										if (auxInvoice.getcBpartnerLocationId() == null) {
											log.warn("No se guardo la direccion del cliente en la BDD, intentelo de nuevo");
											this.migrarDocumento = false;
											conPostgres.rollback();
											break;
										}
										
										bddPostgres.setAuxInvoice(auxInvoice);
									}
								}
								
								auxInvoice.setDescription("");
								
								bddPostgres.setAuxInvoice(auxInvoice);
								
								auxInvoice.setAutorization(auxClaveProvisional);
								
								if (auxInvoice.getAutorization().equals("null") || auxInvoice.getAutorization().equals("")) {
									auxInvoice.setAutorization(auxInvoice.getClaveacceso());
								}
								
								bddPostgres.setAuxInvoice(auxInvoice);
								
								auxInvoice.setTotal(documentos.get(0).getFctrpgdo());
								
								bddPostgres.setAuxInvoice(auxInvoice);
								
								auxInvoice.setServicio(Double.valueOf("0"));
								
								bddPostgres.setAuxInvoice(auxInvoice);
								
								auxInvoice.setcInvoiceId(bddPostgres.putCInvoiceId(conPostgres));
								
								if (auxInvoice.getcInvoiceId() == null) {
									log.warn("No se guardo el documento en la BDD, intentelo de nuevo");
									this.migrarDocumento = false;
									conPostgres.rollback();
									break;
								}
								
								List<DocumentoLinea> lineas = bddOracle.getDocumentoLineas(documentos.get(0).getFctrcdgo(), documentos.get(0).getCliid(), conOracle);
								
								int linea = 0;
								
								if (!lineas.isEmpty()) {
									for (int i=0; i<lineas.size(); i++) {
										InvoiceLine auxInvoiceLine = new InvoiceLine();
										
										linea = linea + 10;
										auxInvoiceLine.setLinea(linea);
										
										List<String> auxMProductId = bddPostgres.getMProductId(lineas.get(i).getItemcdgodtfc(), conPostgres);
										
										if (auxMProductId.isEmpty()) {
											List<String> auxProductCatergoryId = bddPostgres.getMProductCategoryId(this.configuracion.getCategoriaproducto(), conPostgres); 
											
											if (auxProductCatergoryId.isEmpty()) {
												auxInvoiceLine.setmProductCategoryId(bddPostgres.putMProductCategoryId(this.configuracion.getCategoriaproducto(), conPostgres));
												
												if (auxInvoiceLine.getmProductCategoryId() == null) {
													log.warn("No se guardo la categoria del producto c�digo " + this.configuracion.getCategoriaproducto() + " en la BDD");
													this.migrarDocumento = false;
													conPostgres.rollback();
													break;
												}
											} else if (auxProductCatergoryId.size() != 1) {
												log.warn("Hay m�s de una categor�a del producto " + configuracion.getCategoriaproducto() + " en la BDD");
												this.migrarDocumento = false;
												conPostgres.rollback();
												break;
											} else {
												auxInvoiceLine.setmProductCategoryId(auxProductCatergoryId.get(0));
												auxProductCatergoryId = null;
											}
											
											auxInvoiceLine.setmProductId(bddPostgres.putMProductId(auxInvoiceLine.getmProductCategoryId(), lineas.get(i), conPostgres));
											
											if (auxInvoiceLine.getmProductId() == null) {
												log.warn("No se guardo el producto con este c�digo " + lineas.get(i).getItemcdgodtfc() + " en la BDD");
												this.migrarDocumento = false;
												conPostgres.rollback();
												break;
											}
										} else if (auxMProductId.size() != 1) {
											log.warn("Hay m�s de un producto con el mismo c�digo " + lineas.get(i).getItemcdgodtfc() + " en la BDD");
											this.migrarDocumento = false;
											conPostgres.rollback();
											break;
										} else {
											auxInvoiceLine.setmProductId(auxMProductId.get(0));
											auxMProductId = null;
										}
										
										auxInvoiceLine.setCantidad(lineas.get(i).getDtfccntd());
										auxInvoiceLine.setPrecioUnitario(lineas.get(i).getDtfcpcsimp());
										auxInvoiceLine.setDescuento("0");
										auxInvoiceLine.setServicio("0");
										
										DecimalFormat format2 = new DecimalFormat("0.0000");
										DecimalFormat format1 = new DecimalFormat("0");
										double auxPorcentajeDescuento = 0;
										
										if (Double.valueOf(auxInvoiceLine.getPrecioUnitario()) != 0) {
											auxPorcentajeDescuento = Double.valueOf(auxInvoiceLine.getDescuento()) * (100/Double.valueOf(auxInvoiceLine.getPrecioUnitario()));
										}
										
										auxInvoiceLine.setPorcentajeDescuento(format2.format(auxPorcentajeDescuento).replace(",", "."));
										auxInvoiceLine.setTotalLinea(lineas.get(i).getTotallinea());
										
										auxInvoiceLine.setValor(lineas.get(i).getDtfcvaliva());
										auxInvoiceLine.setTotalLineaSinImpuestos(lineas.get(i).getDtfctot());
										
										double auxSubTotalLinea = Double.valueOf(auxInvoiceLine.getTotalLineaSinImpuestos());
										double auximpuesto = Double.valueOf(auxInvoiceLine.getValor());
										
										double auxPorcentajeImpuesto = 0;
										
										if (auxSubTotalLinea != 0) {
											auxPorcentajeImpuesto = (auximpuesto * 100) / auxSubTotalLinea;
											auxPorcentajeImpuesto = Double.valueOf(format1.format(auxPorcentajeImpuesto).replaceAll(",", "."));
										}
										
										if (auxInvoice.getServicio() > 0) {
											if (auxPorcentajeImpuesto == 0) {
												auxInvoiceLine.setPorcentaje("SERVICIO+IVA0%");
											}else if (auxPorcentajeImpuesto == 12) {
												auxInvoiceLine.setPorcentaje("SERVICIO+IVA12%");
											}else if (auxPorcentajeImpuesto == 14) {
												auxInvoiceLine.setPorcentaje("SERVICIO+IVA14%");
											}
										}else if (auxPorcentajeImpuesto == 0) {
											auxInvoiceLine.setPorcentaje("IVA 0%");
										}else if (auxPorcentajeImpuesto == 12) {
											auxInvoiceLine.setPorcentaje("IVA 12%");
										}else if (auxPorcentajeImpuesto == 14) {
											auxInvoiceLine.setPorcentaje("IVA 14%");
										}
										
										List<String> auxCTaxId = bddPostgres.getCTaxId(auxInvoiceLine.getPorcentaje(), conPostgres);
										
										if (auxCTaxId.isEmpty()) {
											log.warn("No hay el impuesto " + auxInvoiceLine.getPorcentaje() + " en la BDD");
											this.migrarDocumento = false;
											conPostgres.rollback();
											break;
										} else if (auxCTaxId.size() != 1) {
											log.warn("Hay m�s de un impuesto " + auxInvoiceLine.getPorcentaje() + " en la BDD");
											this.migrarDocumento = false;
											conPostgres.rollback();
											break;
										} else {
											auxInvoiceLine.setcTaxId(auxCTaxId.get(0));
											auxCTaxId = null;
										}
										
										auxInvoiceLine.setcInvoiceLineId(bddPostgres.putCInvoiceLineId(auxInvoiceLine, conPostgres));
										
										auxInvoiceLineas.add(auxInvoiceLine);
									}
									
									if (!this.migrarDocumento) {
										break;
									}
									
									if (!bddPostgres.procesarDocumento(null, auxInvoiceLineas, conPostgres)) {
										log.warn("# documento: " + documentos.get(0).getFctrnmro() + ", " +bddPostgres.getMensajeError());
										this.migrarDocumento = false;
										conPostgres.rollback();
										break;
									}
								} else {
									log.warn("No hay lineas en el documento: " + documentos.get(0).getFctrnmro());
									this.migrarDocumento = false;
									break;
								}
								
								String auxDomicilio = bddPostgres.putDomicilioId(documentos.get(0), conPostgres);
								
								if (auxDomicilio == null) {
									conPostgres.rollback();
									conOracle.rollback();
									break;
								}
								
								if (!bddOracle.actualizarDocumento(documentos.get(0), "Y", conOracle)) {
									conPostgres.rollback();
									conOracle.rollback();
									break;
								}
								
								if (this.migrarDocumento) {
									conOracle.commit();
									conPostgres.commit();
									log.info("Documento guardado:" + documentos.get(0).getFctrnmro());
								}
							}
						}
					} catch (Exception ex) {
						log.warn(ex.getMessage());
						conOracle.rollback();
						conPostgres.rollback();
					} finally {
						if (conOracle != null) try {conOracle.close(); conOracle = null;} catch (SQLException e) {};
						if (conPostgres != null) try {conPostgres.close(); conPostgres = null;} catch (SQLException e) {};
						this.exception = bddPostgres.isException();
						this.exception = bddOracle.isException();
					}
				} else {
					if (conOracle != null) try {conOracle.close(); conOracle = null;} catch (SQLException e) {};
					if (conPostgres != null) try {conPostgres.close(); conPostgres = null;} catch (SQLException e) {};
					this.exception = bddPostgres.isException();
					this.exception = bddOracle.isException();
				}
			} catch (Exception ex) {
				// TODO: handle exception
				log.warn(ex.getMessage());
				this.monitorearDomicilios = false;
			} finally {
				if (this.exception) {
					this.dataSourceOracle = null;
					this.dataSourcePostgres = null;
				}
			}
		}
	}
}
