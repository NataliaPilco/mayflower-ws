package com.atrums.persistencia;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.tomcat.dbcp.dbcp.BasicDataSource;

import com.atrums.modelo.Configuracion;

public class ConexionOracle {
	
	static final Logger log = Logger.getLogger(ConexionPostgres.class);
	private static Configuracion configuracion = new Configuracion();
	private static DataSource dataSource = null;
	
	static {
		log.info("Inside Database() static method... ");
		BasicDataSource basicDataSource = new BasicDataSource();
		
		try {
			basicDataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
			basicDataSource.setUsername(configuracion.getUsuariooracle());
			basicDataSource.setPassword(configuracion.getPasswordoracle());
			basicDataSource.setUrl("jdbc:oracle:thin:@" + 
					configuracion.getIporacle() + ":" + 
					configuracion.getPuertooracle() + "/" + 
					configuracion.getBddoracle());
			basicDataSource.setMaxActive(10);
			basicDataSource.setMaxIdle(10);
			basicDataSource.setInitialSize(1);
			basicDataSource.setMaxWait(10000);
			basicDataSource.setRemoveAbandonedTimeout(300);
			
			dataSource = basicDataSource;
		} catch (Exception ex) {log.warn(ex.getMessage());}
	}
	
	public static DataSource getDataSource() {
		return dataSource;
	}

	public static void setDataSource(DataSource dataSource) {
		ConexionOracle.dataSource = dataSource;
	}
}
