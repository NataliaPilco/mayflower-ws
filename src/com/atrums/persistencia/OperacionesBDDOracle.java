package com.atrums.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import com.atrums.modelo.Documento;
import com.atrums.modelo.DocumentoLinea;

public class OperacionesBDDOracle {
	static final Logger log = Logger.getLogger(OperacionesBDDOracle.class);
	private String mensajeError = "";
	private boolean exception = false;
	
	public String getMensajeError() {
		return mensajeError;
	}

	public boolean isException() {
		return exception;
	}

	public void setException(boolean exception) {
		this.exception = exception;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	
	public Connection getConneccion(DataSource dataSource) {
		// TODO Auto-generated method stub
		Connection connection = null;
				
		try {
			connection = dataSource.getConnection();
			connection.setAutoCommit(false);
					
			if (connection.isClosed()) {
				connection = null;
			}
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		}
				
		return connection;
	}
	
	public List<Documento> getDocumento(Connection connection){
		PreparedStatement ps = null;
		String sql;
		List<Documento> documentos = new ArrayList<Documento>();
		
		ResultSet rs = null;
		try {
			sql = "SELECT CI.FCTRCDGO, "
					+ "CI.BAN_ERP, "
					+ "TO_CHAR(CI.FCTRFCHA, 'ddmmyyyy') FCTRFCHA, "
					+ "CI.FCTRNMRO, "
					+ "CI.ETDOCDGO, "
					+ "CI.CLNTCDGOFCTR, "
					+ "CI.FCTRPRSN, "
					+ "CI.FCTRDIRE, "
					+ "CI.FCTR_CLNT_MAIL, "
					+ "CI.FCTRPGDO, "
					+ "CI.CLI_ID "
					+ "FROM VD_CABECERA_FACTURA CI "
					+ "WHERE ROWNUM <= 1 "
					+ "AND CI.ETDOCDGO = 'S' "
					+ "AND CI.BAN_ERP IN ('N','P') "
					+ "ORDER BY CI.BAN_ERP, CI.FCTRFCHA";
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				Documento documento = new Documento();
				
				documento.setFctrcdgo(rs.getString("FCTRCDGO"));
				documento.setBan_erp(rs.getString("BAN_ERP"));
				documento.setFctrfcha(rs.getString("FCTRFCHA"));
				documento.setFctrnmro(rs.getString("FCTRNMRO"));
				documento.setEtdocdgo(rs.getString("ETDOCDGO"));
				documento.setClntcdgofctr(rs.getString("CLNTCDGOFCTR"));
				documento.setFctrprsn(rs.getString("FCTRPRSN"));
				documento.setFctrdire(rs.getString("FCTRDIRE"));
				documento.setFctrclntmail(rs.getString("FCTR_CLNT_MAIL"));
				documento.setFctrpgdo(rs.getString("FCTRPGDO"));
				documento.setCliid(rs.getString("CLI_ID"));
				
				documentos.add(documento);
			}
			
			if (documentos.size() > 0) {
				sql = "UPDATE VD_CABECERA_FACTURA SET BAN_ERP = 'P' "
						+ "WHERE FCTRCDGO = '" + documentos.get(0).getFctrcdgo() + "' "
						+ "AND FCTRNMRO = '" + documentos.get(0).getFctrnmro() + "' "
						+ "AND CLI_ID = '" + documentos.get(0).getCliid() + "'";
				ps = connection.prepareStatement(sql);
				int registro = ps.executeUpdate();
				
				if (registro == 1) {
					connection.commit();
				}
			}
			
			rs.close();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (ps != null) ps.close(); } catch (Exception e) {};
		}
		
		return documentos;
	}
	
	public List<DocumentoLinea> getDocumentoLineas(String documentid, String cliid, Connection connection){
		PreparedStatement ps = null;
		String sql;
		List<DocumentoLinea> documentosLineas = new ArrayList<DocumentoLinea>();
		
		ResultSet rs = null;
		try {
			sql = "SELECT CIL.ITEMCDGODTFC, "
					+ "CIL.DTFCDSCR, "
					+ "CIL.DTFCCNTD, "
					+ "CIL.DTFCPCSIMP, "
					+ "CIL.DTFCPCUN, "
					+ "CIL.DTFCTOT, "
					+ "CIL.DTFCVALIVA, "
					+ "(CIL.DTFCTOT + CIL.DTFCVALIVA) AS TOTALLINEA "
					+ "FROM VD_DETALLE_FACTURAS CIL "
					+ "WHERE CIL.FCTRCDGO = '" + documentid + "' "
					+ "AND CIL.CLI_ID = '" + cliid + "'";
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				DocumentoLinea documento = new DocumentoLinea();
				
				documento.setItemcdgodtfc(rs.getString("ITEMCDGODTFC"));
				documento.setDtfcdscr(rs.getString("DTFCDSCR").replaceAll("'", ""));
				documento.setDtfccntd(rs.getString("DTFCCNTD"));
				documento.setDtfcpcsimp(rs.getString("DTFCPCSIMP"));
				documento.setDtfcpcun(rs.getString("DTFCPCUN"));
				documento.setDtfctot(rs.getString("DTFCTOT"));
				documento.setDtfcvaliva(rs.getString("DTFCVALIVA"));
				documento.setTotallinea(rs.getString("TOTALLINEA"));
				
				documentosLineas.add(documento);
			}
			
			rs.close();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (ps != null) ps.close(); } catch (Exception e) {};
		}
		
		return documentosLineas;
	}
	
	public boolean actualizarDocumento(Documento documento, String estado, Connection connection) {
		// TODO Auto-generated method stub
		PreparedStatement ps = null;
		String sql;
		int registro = 0;
		
		try {
			sql = "UPDATE VD_CABECERA_FACTURA SET BAN_ERP = '" + estado + "' "
					+ "WHERE FCTRCDGO = '" + documento.getFctrcdgo() + "' "
					+ "AND FCTRNMRO = '" + documento.getFctrnmro() + "' "
					+ "AND CLI_ID = '" + documento.getCliid() + "'";
			ps = connection.prepareStatement(sql);
			registro = ps.executeUpdate();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (ps != null) ps.close(); } catch (Exception e) {};
		}
		
		if (registro > 0) {
			return true;
		}else{
			return false;
		}
	}
}
