package com.atrums.persistencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.apache.sling.commons.json.JSONObject;

import com.atrums.modelo.Documento;
import com.atrums.modelo.DocumentoLinea;
import com.atrums.modelo.Invoice;
import com.atrums.modelo.InvoiceLine;
import com.atrums.modelo.Pago;

public class OperacionesBDDPostgres {
	static final Logger log = Logger.getLogger(OperacionesBDDPostgres.class);
	private Invoice auxInvoice;
	private String mensajeError = "";
	private boolean exception = false;
	
	public OperacionesBDDPostgres() {
		super();
	}

	public boolean isException() {
		return exception;
	}

	public void setException(boolean exception) {
		this.exception = exception;
	}

	public String getMensajeError() {
		return mensajeError;
	}

	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public Invoice getAuxInvoice() {
		return auxInvoice;
	}

	public void setAuxInvoice(Invoice auxInvoice) {
		this.auxInvoice = auxInvoice;
	}

	public Connection getConneccion(DataSource dataSource) {
		// TODO Auto-generated method stub
		Connection connection = null;
				
		try {
			connection = dataSource.getConnection();
			connection.setAutoCommit(false);
					
			if (connection.isClosed()) {
				connection = null;
			}
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
		}
				
		return connection;
	}
	
	public boolean isExite(String sql, Connection connection) {
		Statement statement = null;
		boolean auxRespuesta = true;
		
		ResultSet rs = null;
		try {
			statement = connection.createStatement();
			
			rs = statement.executeQuery(sql);
			
			while (rs.next()) {
				if (rs.getInt("total") == 0) {
					auxRespuesta = false;
				}
			}
			
			rs.close();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (statement != null) statement.close(); } catch (Exception e) {};
		}
		
		return auxRespuesta;
	}
	
	public List<String> getAdClientId(String empresa, Connection connection) {
		PreparedStatement ps = null;
		String sql;
		List<String> adClientId = new ArrayList<String>();
		
		ResultSet rs = null;
		try {
			sql = "SELECT ac.ad_client_id "
					+ "FROM ad_client ac "
					+ "WHERE upper(ac.name) LIKE '%" + empresa.toUpperCase() + "%' "
					+ "AND ac.isactive = 'Y';";
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				adClientId.add(rs.getString("ad_client_id"));
			}
			
			rs.close();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (ps != null) ps.close(); } catch (Exception e) {};
		}
		
		return adClientId;
	}
	
	public List<String> getAdOrgId(Connection connection) {
		PreparedStatement ps = null;
		String sql;
		List<String> adOrgId = new ArrayList<String>();
		
		ResultSet rs = null;
		try {
			sql = "SELECT ao.ad_org_id "
					+ "FROM ad_org ao "
					+ "WHERE ao.em_co_nro_estab = '" + this.auxInvoice.getSucursal() + "' "
					+ "AND ao.em_co_punto_emision = '" + this.auxInvoice.getPtoemision() + "' "
					+ "AND ao.ad_client_id = '" + this.auxInvoice.getAdClientId() + "' "
					+ "AND ao.isactive = 'Y';";
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				adOrgId.add(rs.getString("ad_org_id"));
			}
			
			rs.close();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (ps != null) ps.close(); } catch (Exception e) {};
		}
		
		return adOrgId;
	}
	
	public List<String> getCDoctypeId(String documento, Connection connection) {
		PreparedStatement ps = null;
		String sql;
		List<String> cDoctypeId = new ArrayList<String>();
		
		ResultSet rs = null;
		try {
			sql = "SELECT cdt.c_doctype_id "
					+ "FROM c_doctype cdt "
					+ "WHERE cdt.ad_client_id = '" + this.auxInvoice.getAdClientId() + "' "
					+ "AND cdt.ad_org_id = '" + this.auxInvoice.getAdOrgId() + "' "
					+ "AND upper(cdt.name) LIKE '" + documento.toUpperCase() + "%' "
					+ "AND cdt.isactive = 'Y' "
					+ "AND issotrx = 'Y' ";
			
			if (documento.equals("FACTURA")) {
				sql = sql + "AND isreturn = 'N';";
			} else {
				sql = sql + ";";
			}
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				cDoctypeId.add(rs.getString("c_doctype_id"));
			}
			
			rs.close();
			
			if (cDoctypeId.size() == 0) {
				sql = "SELECT cdt.c_doctype_id "
						+ "FROM c_doctype cdt "
						+ "WHERE cdt.ad_client_id = '" + this.auxInvoice.getAdClientId() + "' "
						+ "AND upper(cdt.name) LIKE '" + documento.toUpperCase() + "%' "
						+ "AND cdt.isactive = 'Y' "
						+ "AND issotrx = 'Y' ";
				
				if (documento.equals("FACTURA")) {
					sql = sql + "AND isreturn = 'N';";
				} else {
					sql = sql + ";";
				}
				ps = connection.prepareStatement(sql);
				rs = ps.executeQuery();
				
				while (rs.next()) {
					cDoctypeId.add(rs.getString("c_doctype_id"));
				}
				
				rs.close();
			}
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (ps != null) ps.close(); } catch (Exception e) {};
		}
		
		return cDoctypeId;
	}
	
	public List<String> getCBpartnerId(String ciRuc, Connection connection) {
		PreparedStatement ps = null;
		String sql;
		List<String> cBpartnerId = new ArrayList<String>();
		
		ResultSet rs = null;
		try {
			sql = "SELECT cb.c_bpartner_id "
					+ "FROM c_bpartner cb "
					+ "WHERE cb.value = '" + ciRuc + "' "
					+ "AND cb.ad_client_id = '" + this.auxInvoice.getAdClientId() + "';";
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				cBpartnerId.add(rs.getString("c_bpartner_id"));
			}
			
			rs.close();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (ps != null) ps.close(); } catch (Exception e) {};
		}
		
		return cBpartnerId;
	}
	
	public List<String> getFinPaymentmethodId(String metodo, Connection connection) {
		Statement statement = null;
		String sql;
		List<String> finPaymentmethodId = new ArrayList<String>();
		
		ResultSet rs = null;
		try {
			statement = connection.createStatement();
			
			sql = "SELECT fpm.fin_paymentmethod_id "
					+ "FROM fin_paymentmethod fpm "
					+ "WHERE fpm.isactive = 'Y' "
					+ "AND fpm.ad_client_id = '" + this.auxInvoice.getAdClientId() + "' "
					+ "AND fpm.ad_org_id = '0' "
					+ "AND upper(fpm.name) LIKE '" + metodo.toUpperCase() + "' "
					+ "LIMIT 1;";
			
			rs = statement.executeQuery(sql);
			
			while (rs.next()) {
				finPaymentmethodId.add(rs.getString("fin_paymentmethod_id"));
			}
			
			rs.close();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (statement != null) statement.close(); } catch (Exception e) {};
		}
		
		return finPaymentmethodId;
	}
	
	public List<String> getFinPaymentScheduleId(Connection connection) {
		Statement statement = null;
		String sql;
		List<String> finPaymentScheduleId = new ArrayList<String>();
		
		ResultSet rs = null;
		try {
			statement = connection.createStatement();
			
			sql = "SELECT fps.fin_payment_schedule_id "
					+ "FROM fin_payment_schedule fps "
					+ "WHERE fps.c_invoice_id = '" + this.auxInvoice.getcInvoiceId() + "';";
			
			rs = statement.executeQuery(sql);
			
			while (rs.next()) {
				finPaymentScheduleId.add(rs.getString("fin_payment_schedule_id"));
			}
			
			rs.close();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (statement != null) statement.close(); } catch (Exception e) {};
		}
		
		return finPaymentScheduleId;
	}
	
	public List<String> getMProductId(String codigo, Connection connection) {
		Statement statement = null;
		String sql;
		List<String> mProductId = new ArrayList<String>();
		
		ResultSet rs = null;
		try {
			statement = connection.createStatement();
			
			sql = "SELECT mp.m_product_id "
					+ "FROM m_product AS mp "
					+ "WHERE upper(mp.value) = '" + codigo + "' "
					+ "AND mp.ad_client_id = '" + this.auxInvoice.getAdClientId() + "' "
					+ "AND upper(mp.name) NOT LIKE 'NULL';";
			
			rs = statement.executeQuery(sql);
			
			while (rs.next()) {
				mProductId.add(rs.getString("m_product_id"));
			}
			
			rs.close();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (statement != null) statement.close(); } catch (Exception e) {};
		}
		
		return mProductId;
	}
	
	public List<String> getMProductCategoryId(String value, Connection connection) {
		Statement statement = null;
		String sql;
		List<String> mProductCategoryId = new ArrayList<String>();
		
		ResultSet rs = null;
		try {
			statement = connection.createStatement();
			
			sql = "SELECT m_product_category_id "
					+ "FROM m_product_category mpc "
					+ "WHERE upper(mpc.value) = '" + value.toUpperCase() + "';";
			
			rs = statement.executeQuery(sql);
			
			while (rs.next()) {
				mProductCategoryId.add(rs.getString("m_product_category_id"));
			}
			
			rs.close();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (statement != null) statement.close(); } catch (Exception e) {};
		}
		
		return mProductCategoryId;
	}
	
	public List<String> getCTaxId(String porcentaje, Connection connection) {
		Statement statement = null;
		String sql;
		List<String> cTaxId = new ArrayList<String>();
		
		ResultSet rs = null;
		try {
			statement = connection.createStatement();
			
			sql = "SELECT ct.c_tax_id "
					+ "FROM c_tax ct "
					+ "WHERE upper(ct.name) = '" + porcentaje + "' "
					+ "AND ad_client_id = '" + this.auxInvoice.getAdClientId() + "';";
			
			rs = statement.executeQuery(sql);
			
			while (rs.next()) {
				cTaxId.add(rs.getString("c_tax_id"));
			}
			
			rs.close();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (statement != null) statement.close(); } catch (Exception e) {};
		}
		
		return cTaxId;
	}
	
	public String getCBpartnerLocationId(Connection connection) {
		Statement statement = null;
		String sql;
		String cBpartnerLocationId = null;
		
		ResultSet rs = null;
		try {
			statement = connection.createStatement();
			
			sql = "SELECT cbl.c_bpartner_location_id "
					+ "FROM c_bpartner_location cbl "
					+ "WHERE cbl.c_bpartner_id = '" + this.auxInvoice.getcBpartnerId() + "' LIMIT 1;";
			
			rs = statement.executeQuery(sql);
			
			while (rs.next()) {
				cBpartnerLocationId = rs.getString("c_bpartner_location_id");
			}
			
			rs.close();
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (statement != null) statement.close(); } catch (Exception e) {};
		}
		
		return cBpartnerLocationId;
	}
	
	public String putCBpartnerId(String nombreCliente, String emailCliente, String rucCi, String tipoRucCi, Connection connection) {
		PreparedStatement ps = null;
		String sql;
		int registro = 0;
		
		String cBpartnerId = UUID.randomUUID().toString().replace("-", "").toUpperCase();
		
		try {
			sql = "INSERT INTO c_bpartner("
					+ "c_bpartner_id, ad_client_id, ad_org_id, isactive, created, createdby, "
					+ "updated, updatedby, value, name, name2, description, issummary, "
					+ "c_bp_group_id, isonetime, isprospect, isvendor, iscustomer, isemployee, "
					+ "issalesrep, referenceno, duns, url, ad_language, taxid, istaxexempt, "
					+ "c_invoiceschedule_id, rating, salesvolume, numberemployees, naics, "
					+ "firstsale, acqusitioncost, potentiallifetimevalue, actuallifetimevalue, "
					+ "shareofcustomer, paymentrule, so_creditlimit, so_creditused, "
					+ "c_paymentterm_id, m_pricelist_id, isdiscountprinted, so_description, "
					+ "poreference, paymentrulepo, po_pricelist_id, po_paymentterm_id, "
					+ "documentcopies, c_greeting_id, invoicerule, deliveryrule, deliveryviarule, "
					+ "salesrep_id, bpartner_parent_id, socreditstatus, ad_forced_org_id, "
					+ "showpriceinorder, invoicegrouping, fixmonthday, fixmonthday2, "
					+ "fixmonthday3, isworker, upc, c_salary_category_id, invoice_printformat, "
					+ "last_days, po_bankaccount_id, po_bp_taxcategory_id, po_fixmonthday, "
					+ "po_fixmonthday2, po_fixmonthday3, so_bankaccount_id, so_bp_taxcategory_id, "
					+ "fiscalcode, isofiscalcode, po_c_incoterms_id, so_c_incoterms_id, "
					+ "fin_paymentmethod_id, po_paymentmethod_id, fin_financial_account_id, "
					+ "po_financial_account_id, customer_blocking, vendor_blocking, "
					+ "so_payment_blocking, po_payment_blocking, so_invoice_blocking, "
					+ "po_invoice_blocking, so_order_blocking, po_order_blocking, so_goods_blocking, "
					+ "po_goods_blocking, iscashvat, em_co_tipocontrib, em_co_tipo_identificacion, "
					+ "em_co_bp_nro_aut_fc_sri, em_co_bp_fec_venct_aut_fc_sri, em_co_bp_nro_estab, "
					+ "em_co_bp_punto_emision, em_co_bp_nro_aut_rt_sri, em_co_bp_fec_venct_aut_rt_sri, "
					+ "em_co_email, em_co_niv_edu, em_co_vivienda, em_co_anios_res, "
					+ "em_co_meses_res, em_co_total_meses, em_co_fechanac, em_co_ciudadnac, "
					+ "em_co_nacionalidad, em_co_nombres, em_co_apellidos, em_co_com_tarj_cred, "
					+ "em_co_natural_juridico, "
					+ "em_no_fechaingreso, "
					+ "em_no_fechasalida, em_no_estadocivil, em_no_genero, em_no_fechanacimiento, "
					+ "em_no_area_empresa_id, em_no_motivo_salida, em_no_sissalnet, "
					+ "em_no_isdiscapacitado, em_no_istercera_edad)"
					+ "VALUES ("
					+ "'" + cBpartnerId + "', '" + this.auxInvoice.getAdClientId() + "', '0', 'Y', now(), '100', "
					+ "now(), '100', '" + rucCi + "', initcap('" + nombreCliente + "'), initcap('" + nombreCliente + "'), initcap('" + nombreCliente + "'), 'N', "
					+ "(SELECT c_bp_group_id FROM c_bp_group AS cbg WHERE upper(cbg.name) LIKE '%CLIENTES%' AND ad_client_id = '" + this.auxInvoice.getAdClientId() + "' LIMIT 1), 'N', 'N', 'N', 'Y', 'N', "
					+ "'N', null, null, null, 'es_EC', '" + rucCi + "', 'N', "
					+ "null, null, null, null, null, "
					+ "null, '0', '0', '0', "
					+ "null, null, '0', '0', "
					+ "null, null, 'N', null, "
					+ "null, null, null, null, "
					+ "null, null, 'I', null, null, "
					+ "null, null, 'O', null, "
					+ "'Y', '000000000000000', null, null, "
					+ "null, 'N', null, null, null, "
					+ "'100', null, null, null, "
					+ "null, null, null, null, "
					+ "null, null, null, null, "
					+ "null, null, null, "
					+ "null, 'N', 'N', "
					+ "'N', 'Y', 'Y', "
					+ "'Y', 'Y', 'Y', 'Y', "
					+ "'N', 'N', null, '" + tipoRucCi + "', "
					+ "null, null, null, "
					+ "null, null, null, "
					+ "lower('" + emailCliente + "'), null, null, null, "
					+ "null, null, null, null, "
					+ "null, initcap('" + nombreCliente + "'), initcap('" + nombreCliente + "'), null, "
					+ "'PN', "
					+ "null, "
					+ "null, null, null, null, "
					+ "null, null, null, "
					+ "'N', 'N');";
			ps = connection.prepareStatement(sql);
			registro = ps.executeUpdate();
			
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (ps != null) ps.close(); } catch (Exception e) {};
		}
		
		if (registro > 0) {
			return cBpartnerId;
		}else{
			return null;
		}
	}
	
	public String putAdUserId(String nombreCliente, String emailCliente , String teleCliente, Connection connection) {
		PreparedStatement ps = null;
		String sql;
		int registro = 0;
		
		String adUserId = UUID.randomUUID().toString().replace("-", "").toUpperCase();
		
		try {
			sql = "INSERT INTO ad_user("
					+ "ad_user_id, ad_client_id, ad_org_id, isactive, created, createdby, "
					+ "updated, updatedby, name, description, password, email, supervisor_id, "
					+ "c_bpartner_id, processing, emailuser, emailuserpw, c_bpartner_location_id, "
					+ "c_greeting_id, title, comments, phone, phone2, fax, lastcontact, "
					+ "lastresult, birthday, ad_orgtrx_id, firstname, lastname, username, "
					+ "default_ad_client_id, default_ad_language, default_ad_org_id, "
					+ "default_ad_role_id, default_m_warehouse_id, islocked, ad_image_id, "
					+ "grant_portal_access)"
					+ "VALUES ("
					+ "'" + adUserId + "', '" + this.auxInvoice.getAdClientId() + "', '0', 'Y', now(), '100', "
					+ "now(), '100', initcap('" + nombreCliente + "'), null, null, lower('" + emailCliente  + "'), null, "
					+ "'" + this.auxInvoice.getcBpartnerId() + "', 'N', null, null, null, "
					+ "null, null, null, '" + teleCliente + "', null, null, null, "
					+ "null, null, null, initcap('" + nombreCliente + "'), initcap('" + nombreCliente + "'), null, "
					+ "null, null, null, "
					+ "null, null, 'N', null, "
					+ "'N');";
			ps = connection.prepareStatement(sql);
			registro = ps.executeUpdate();
			
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (ps != null) ps.close(); } catch (Exception e) {};
		}
		
		if (registro > 0) {
			return adUserId;
		}else{
			return null;
		}
	}
	
	public String putCLocationId(String direccion, Connection connection) {
		PreparedStatement ps = null;
		String sql;
		int registro = 0;
		
		String cLocationId = UUID.randomUUID().toString().replace("-", "").toUpperCase();
		
		try {
			sql = "INSERT INTO c_location("
					+ "c_location_id, ad_client_id, ad_org_id, isactive, created, createdby, "
					+ "updated, updatedby, address1, address2, city, postal, postal_add, "
					+ "c_country_id, c_region_id, c_city_id, regionname, em_co_parroquia, "
					+ "em_co_barrio) "
					+ "VALUES ("
					+ "'" + cLocationId + "', '" + this.auxInvoice.getAdClientId() + "', '0', 'Y', now(), '100', "
					+ "now(), '100', initcap('" + direccion + "'), null, null, null, null, "
					+ "(SELECT c_country_id FROM c_country AS cc WHERE upper(cc.name) LIKE 'ECUADOR'), null, null, null, null, "
					+ "null);";
			ps = connection.prepareStatement(sql);
			registro = ps.executeUpdate();
			
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (ps != null) ps.close(); } catch (Exception e) {};
		}
		
		if (registro > 0) {
			return cLocationId;
		}else{
			return null;
		}
	}
	
	public String putCBpartnerLocationId(String direccion, Connection connection) {
		PreparedStatement ps = null;
		String sql;
		int registro = 0;
		
		String cBpartnerLocationId = UUID.randomUUID().toString().replace("-", "").toUpperCase();
		
		try {
			sql = "INSERT INTO c_bpartner_location("
					+ "c_bpartner_location_id, ad_client_id, ad_org_id, isactive, created, "
					+ "createdby, updated, updatedby, name, isbillto, isshipto, ispayfrom, "
					+ "isremitto, phone, phone2, fax, c_salesregion_id, c_bpartner_id, "
					+ "c_location_id, istaxlocation, upc, em_no_num_dir) "
					+ "VALUES ("
					+ "'" + cBpartnerLocationId + "', '" + this.auxInvoice.getAdClientId() + "', '0', 'Y', now(), "
					+ "'100', now(), '100', initcap('" + direccion + "'), 'Y', 'Y', 'Y', "
					+ "'Y', null, null, null, null, '" + this.auxInvoice.getcBpartnerId() + "', "
					+ "'" + this.auxInvoice.getcLocationId() + "', 'N', null, null);";
			ps = connection.prepareStatement(sql);
			registro = ps.executeUpdate();
			
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (ps != null) ps.close(); } catch (Exception e) {};
		}
		
		if (registro > 0) {
			return cBpartnerLocationId;
		}else{
			return null;
		}
	}
	
	public String putCInvoiceId(Connection connection) {
		PreparedStatement ps = null;
		String sql;
		int registro = 0;
		
		String cInvoiceId = UUID.randomUUID().toString().replace("-", "").toUpperCase();
		
		try {
			sql = "INSERT INTO c_invoice("
						+ "c_invoice_id, ad_client_id, ad_org_id, isactive, created, createdby, "
						+ "updated, updatedby, issotrx, documentno, docstatus, docaction, "
						+ "processing, processed, posted, c_doctype_id, c_doctypetarget_id, "
						+ "c_order_id, description, isprinted, salesrep_id, dateinvoiced, "
						+ "dateprinted, dateacct, c_bpartner_id, c_bpartner_location_id, "
						+ "poreference, isdiscountprinted, dateordered, c_currency_id, paymentrule, "
						+ "c_paymentterm_id, c_charge_id, chargeamt, totallines, grandtotal, "
						+ "m_pricelist_id, istaxincluded, c_campaign_id, c_project_id, c_activity_id, "
						+ "createfrom, generateto, ad_user_id, copyfrom, isselfservice, "
						+ "ad_orgtrx_id, user1_id, user2_id, withholdingamount, taxdate, "
						+ "c_withholding_id, ispaid, totalpaid, outstandingamt, daystilldue, "
						+ "dueamt, lastcalculatedondate, updatepaymentmonitor, fin_paymentmethod_id, "
						+ "fin_payment_priority_id, finalsettlement, daysoutstanding, percentageoverdue, "
						+ "c_costcenter_id, calculate_promotions, a_asset_id, iscashvat, "
						+ "em_co_nro_aut_sri, em_co_vencimiento_aut_sri, em_co_nro_estab, "
						+ "em_co_punto_emision, em_co_codsustento, em_co_rise, em_co_addpayment, "
						+ "em_atecfe_docstatus, em_atecfe_docaction, em_atecfe_menobserror_sri, "
						+ "em_atecfe_codigo_acc, em_atecfe_documento_xml, em_atecfe_c_invoice_id, "
						+ "em_atecfe_fecha_autori, em_aprm_addpayment, em_aprm_processinvoice) "
						+ "VALUES ("
						+ "'" + cInvoiceId + "', '" + this.auxInvoice.getAdClientId() + "', '" + this.auxInvoice.getAdOrgId() + "', 'Y', now(), '100', "
						+ "now(), '100', 'Y', '" + this.auxInvoice.getDocumentno() + "', 'DR', 'CO', "
						+ "'N', 'N', 'N', '" + this.auxInvoice.getcDoctypeId() + "', '" + this.auxInvoice.getcDoctypeId() + "', "
						+ "null, '" + this.auxInvoice.getDescription() + "', 'N', null, to_timestamp('" + this.auxInvoice.getDateinvoiced() + "','DD/MM/YYYY'), "
						+ "null, to_timestamp('" + this.auxInvoice.getDateinvoiced() + "','DD/MM/YYYY'), '" + this.auxInvoice.getcBpartnerId() + "', '" + this.auxInvoice.getcBpartnerLocationId() + "', "
						+ "null, 'N', null, (SELECT cc.c_currency_id FROM c_currency AS cc WHERE upper(cc.iso_code) LIKE 'USD'), null, "
						+ "(SELECT cp.c_paymentterm_id FROM c_paymentterm AS cp WHERE upper(cp.name) like '%CONTADO%' AND cp.ad_client_id = '" + this.auxInvoice.getAdClientId() + "' LIMIT 1), null, '0', '0', '0', "
						+ "(SELECT m_pricelist_id FROM m_pricelist AS mp WHERE upper(mp.name) LIKE '%VENTA%' AND mp.ad_client_id = '" + this.auxInvoice.getAdClientId() + "' LIMIT 1), 'N', null, null, null, "
						+ "'N', 'N', null, 'N', 'N', "
						+ "null, null, null, '0', null, "
						+ "null, 'N', '0', '0', '0', "
						+ "'0', null, 'N', (SELECT fin_paymentmethod_id FROM fin_paymentmethod as fp WHERE upper(fp.name) LIKE '%EFECTIVO%' AND fp.ad_client_id = '" + this.auxInvoice.getAdClientId() + "' LIMIT 1), "
						+ "null, null, null, '0', "
						+ "null, 'N', null, 'N', "
						+ "'" + this.auxInvoice.getAutorization() + "', to_timestamp('" + this.auxInvoice.getDateinvoiced()  + "','DD/MM/YYYY'), '" + this.auxInvoice.getSucursal() + "', "
						+ "'" + this.auxInvoice.getPtoemision() + "', '06', 'N', 'Y', "
						+ "'AP', 'PD', '', "
						+ "'" + this.auxInvoice.getClaveacceso() + "', decode('" + this.auxInvoice.getDocXML() + "','base64'), null, "
						+ "'" + this.auxInvoice.getDateinvoiced() + "', 'Y', 'CO');";
			ps = connection.prepareStatement(sql);
			registro = ps.executeUpdate();
			
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (ps != null) ps.close(); } catch (Exception e) {};
		}
		
		if (registro > 0) {
			return cInvoiceId;
		}else{
			return null;
		}
	}
	
	public String putMProductCategoryId(String value, Connection connection) {
		PreparedStatement ps = null;
		String sql;
		int registro = 0;
		
		String mProductCategoryId = UUID.randomUUID().toString().replace("-", "").toUpperCase();
		
		try {
			sql = "INSERT INTO m_product_category("
					+ "m_product_category_id, ad_client_id, ad_org_id, isactive, created, "
					+ "createdby, updated, updatedby, value, name, description, isdefault, "
					+ "plannedmargin, a_asset_group_id, ad_image_id, issummary)"
					+ "VALUES ("
					+ "'" + mProductCategoryId + "', '" + this.auxInvoice.getAdClientId() + "', '0', 'Y', now(), "
					+ "'100', now(), '100', '" + value.toUpperCase() + "', '" + value.toUpperCase() + "', null, 'N', "
					+ "'0', null, null, 'N');";
			ps = connection.prepareStatement(sql);
			registro = ps.executeUpdate();
			
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (ps != null) ps.close(); } catch (Exception e) {};
		}
		
		if (registro > 0) {
			return mProductCategoryId;
		}else{
			return null;
		}
	}
	
	public String putDomicilioId(Documento documento, Connection connection) {
		PreparedStatement ps = null;
		String sql;
		int registro = 0;
		
		String smaDocumentodomicilioId = UUID.randomUUID().toString().replace("-", "").toUpperCase();
		
		try {
			sql = "INSERT INTO sma_documentodomicilio("
					+ "sma_documentodomicilio_id, ad_client_id, ad_org_id, isactive, "
					+ "created, createdby, updated, updatedby, documentoid, documentno, "
					+ "fecha, total, cli_id, c_invoice_id) "
					+ "VALUES ('" + smaDocumentodomicilioId + "', '" + this.auxInvoice.getAdClientId()  + "', '" + this.auxInvoice.getAdOrgId() + "', 'Y', "
					+ "NOW(), '100', NOW(), '100', '" + documento.getFctrcdgo() + "', '" + documento.getFctrnmro() + "', "
					+ "'" + this.auxInvoice.getDateinvoiced() + "', '" + this.auxInvoice.getTotal() + "', '" + documento.getCliid() + "', '" + this.auxInvoice.getcInvoiceId() + "');";
			ps = connection.prepareStatement(sql);
			registro = ps.executeUpdate();
			
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (ps != null) ps.close(); } catch (Exception e) {};
		}
		
		if (registro > 0) {
			return smaDocumentodomicilioId;
		}else{
			return null;
		}
	}
	
	public String putMProductId(String mProductCategoryId, DocumentoLinea linea, Connection connection) {
		PreparedStatement ps = null;
		String sql;
		int registro = 0;
		
		String mProductId = UUID.randomUUID().toString().replace("-", "").toUpperCase();
		
		try {
			sql = "INSERT INTO m_product("
						+ "m_product_id, ad_client_id, ad_org_id, isactive, created, createdby, "
						+ "updated, updatedby, value, name, description, documentnote, help, "
						+ "upc, sku, c_uom_id, salesrep_id, issummary, isstocked, ispurchased, "
						+ "issold, isbom, isinvoiceprintdetails, ispicklistprintdetails, "
						+ "isverified, m_product_category_id, classification, volume, weight, "
						+ "shelfwidth, shelfheight, shelfdepth, unitsperpallet, c_taxcategory_id, "
						+ "s_resource_id, discontinued, discontinuedby, processing, s_expensetype_id, "
						+ "producttype, imageurl, descriptionurl, guaranteedays, versionno, "
						+ "m_attributeset_id, m_attributesetinstance_id, downloadurl, m_freightcategory_id, "
						+ "m_locator_id, ad_image_id, c_bpartner_id, ispriceprinted, name2, "
						+ "costtype, coststd, stock_min, enforce_attribute, calculated, "
						+ "ma_processplan_id, production, capacity, delaymin, mrp_planner_id, "
						+ "mrp_planningmethod_id, qtymax, qtymin, qtystd, qtytype, stockmin, "
						+ "attrsetvaluetype, isquantityvariable, isdeferredrevenue, revplantype, "
						+ "periodnumber, isdeferredexpense, expplantype, periodnumber_exp, "
						+ "defaultperiod, defaultperiod_exp, bookusingpoprice, c_uom_weight_id, "
						+ "m_brand_id, isgeneric, generic_product_id, createvariants, characteristic_desc, "
						+ "updateinvariants, managevariants"
						+ ")"
						+ "VALUES ("
						+ "'" + mProductId + "', '" + this.auxInvoice.getAdClientId() + "', '0', 'Y', now(), '100', "
						+ "now(), '100', '" + linea.getItemcdgodtfc() + "', initcap('" + linea.getDtfcdscr() + "'), initcap('" + linea.getDtfcdscr() + "'), null, null, "
						+ "null, null, (SELECT c_uom_id FROM c_uom AS cu WHERE (upper(name) LIKE '%UNIT%' OR upper(name) LIKE '%UNIDAD%') LIMIT 1), null, 'N', 'Y', 'Y', "
						+ "'N', 'N', 'N', 'N', "
						+ "'N', '" + mProductCategoryId + "', null, '0', '0', "
						+ "null, null, null, null, (SELECT c_taxcategory_id FROM c_taxcategory WHERE (upper(name) like 'IVA 12%') AND ad_client_id = '" + this.auxInvoice.getAdClientId() + "' LIMIT 1), "
						+ "null, 'N', null, 'N', null, "
						+ "'I', null, null, null, null, "
						+ "null, null, null, null, "
						+ "null, null, null, 'Y', null, "
						+ "null, null, null, 'N', 'N', "
						+ "null, 'N', null, null, null, "
						+ "null, null, null, null, 'N', null, "
						+ "null, 'N', 'N', null, "
						+ "null, 'N', null, null, "
						+ "null, null, 'N', null, "
						+ "null, 'N', null, 'N', null, "
						+ "'N', 'N'"
						+ ");";
			ps = connection.prepareStatement(sql);
			registro = ps.executeUpdate();
			
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (ps != null) ps.close(); } catch (Exception e) {};
		}
		
		if (registro > 0) {
			return mProductId;
		}else{
			return null;
		}
	}
	
	public String putCInvoiceLineId(InvoiceLine auxInvoiceLine, Connection connection) {
		PreparedStatement ps = null;
		String sql;
		int registro = 0;
		
		String cInvoiceLineId = UUID.randomUUID().toString().replace("-", "").toUpperCase();
		
		try {
			sql = "INSERT INTO c_invoiceline("
						+ "c_invoiceline_id, ad_client_id, ad_org_id, isactive, created, "
						+ "createdby, updated, updatedby, c_invoice_id, c_orderline_id, "
						+ "m_inoutline_id, line, description, financial_invoice_line, account_id, "
						+ "m_product_id, qtyinvoiced, pricelist, priceactual, pricelimit, "
						+ "linenetamt, c_charge_id, chargeamt, c_uom_id, c_tax_id, s_resourceassignment_id, "
						+ "taxamt, m_attributesetinstance_id, isdescription, quantityorder, "
						+ "m_product_uom_id, c_invoice_discount_id, c_projectline_id, m_offer_id, "
						+ "pricestd, excludeforwithholding, iseditlinenetamt, taxbaseamt, "
						+ "line_gross_amount, gross_unit_price, c_bpartner_id, periodnumber, "
						+ "grosspricestd, a_asset_id, defplantype, grosspricelist, c_project_id, "
						+ "isdeferred, c_period_id, c_costcenter_id, user1_id, user2_id, "
						+ "explode, bom_parent_id, em_atecfe_descuento) "
						+ "VALUES "
						+ "('" + cInvoiceLineId + "', '" + this.auxInvoice.getAdClientId() + "', '" + this.auxInvoice.getAdOrgId() + "', 'Y', now(), "
						+ "'100', now(), '100', '" + this.auxInvoice.getcInvoiceId() + "', null, "
						+ "null, '" + auxInvoiceLine.getLinea() + "', null, 'N', null, "
						+ "'" + auxInvoiceLine.getmProductId() + "', '" + auxInvoiceLine.getCantidad() + "', '" + auxInvoiceLine.getPrecioUnitario() + "', '" + auxInvoiceLine.getPrecioUnitario() + "', '0', "
						+ "'" + auxInvoiceLine.getTotalLineaSinImpuestos() + "', null, '0', (SELECT c_uom_id FROM m_product WHERE m_product_id = '" + auxInvoiceLine.getmProductId() + "' LIMIT 1), '" + auxInvoiceLine.getcTaxId() + "', null, "
						+ "'" + auxInvoiceLine.getValor() + "', null, 'N', null, "
						+ "null, null, null, null, "
						+ "'" + auxInvoiceLine.getPrecioUnitario() + "', 'N', 'N', '" + auxInvoiceLine.getTotalLineaSinImpuestos() + "', "
						+ "'0', '0', null, null, "
						+ "'0', null, null, '0', null, "
						+ "'N', null, null, null, null, "
						+ "'N', null, '" + auxInvoiceLine.getPorcentajeDescuento() + "');";
			ps = connection.prepareStatement(sql);
			registro = ps.executeUpdate();
			
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (ps != null) ps.close(); } catch (Exception e) {};
		}
		
		if (registro > 0) {
			return cInvoiceLineId;
		}else{
			return null;
		}
	}
	
	public String putFinPaymentId(Pago pago, Connection connection) {
		Statement statement = null;
		String sql;
		int registro = 0;
		
		String finPaymentId = UUID.randomUUID().toString().replace("-", "").toUpperCase();
		
		try {
			statement = connection.createStatement();
			
			if (pago.getIdCupon() != null) {
				sql = "INSERT INTO fin_payment("
						+ "fin_payment_id, ad_client_id, ad_org_id, created, createdby, "
						+ "updated, updatedby, isactive, isreceipt, c_bpartner_id, paymentdate, "
						+ "c_currency_id, amount, writeoffamt, fin_paymentmethod_id, documentno, "
						+ "referenceno, status, processed, processing, posted, description, "
						+ "fin_financial_account_id, c_doctype_id, c_project_id, c_campaign_id, "
						+ "c_activity_id, user1_id, user2_id, generated_credit, used_credit, "
						+ "createdbyalgorithm, finacc_txn_convert_rate, finacc_txn_amount, "
						+ "fin_rev_payment_id, c_costcenter_id, em_atecdp_deposito, em_co_nro_cheque, "
						+ "em_co_nombre_banco, em_aprm_process_payment, "
						+ "em_aprm_reconcile_payment, em_aprm_add_scheduledpayments, em_aprm_executepayment, "
						+ "em_aprm_reversepayment, em_sma_id_cupon, em_sma_tipo_pago, em_sma_id_empresa)"
						+ "VALUES ("
						+ "'" + finPaymentId + "', '" + this.auxInvoice.getAdClientId() + "', '" + this.auxInvoice.getAdOrgId() + "', now(), '100', "
						+ "now(), '100', 'Y', 'Y', '" + this.auxInvoice.getcBpartnerId() + "', (SELECT ci.dateinvoiced FROM c_invoice AS ci WHERE ci.c_invoice_id = '" + this.auxInvoice.getcInvoiceId() + "'), "
						+ "'100', '" + pago.getAmount() + "', '0', '" + pago.getFinPaymentmethodId() + "', '" + this.auxInvoice.getDocumentno() + "', "
						+ "null, 'RPR', 'N', 'N', 'N', '" + pago.getReferencia() + "Factura N� : ' || '" + this.auxInvoice.getDocumentno() + "', "
						+ "(SELECT fin_financial_account_id FROM fin_financial_account AS ffa WHERE upper(ffa.name) LIKE '%CAJA%' AND ffa.ad_org_id IN (SELECT ao.ad_org_id " + "FROM ad_org As ao " + "WHERE isactive = 'Y' " + "AND em_co_nro_estab = '" + this.auxInvoice.getSucursal() + "' " + "AND em_co_punto_emision = '001' AND ao.ad_client_id = '" + this.auxInvoice.getAdClientId() + "' LIMIT 1) "
						+ "AND ffa.ad_client_id = '" + this.auxInvoice.getAdClientId() + "' LIMIT 1), (SELECT c_doctype_id FROM c_doctype AS cd WHERE (upper(cd.name) LIKE '%COMPROBANTE DE INGRESO%' OR upper(cd.name) LIKE '%COBRO%') AND cd.ad_client_id = '" + this.auxInvoice.getAdClientId() + "' LIMIT 1), null, null, "
						+ "null, null, null, '0', '0', "
						+ "'N', '1', '" + pago.getAmount() + "', "
						+ "null, null, null, null, "
						+ "null, 'RE', "
						+ "'N', 'N', 'N', "
						+ "'N', '" + pago.getIdCupon() + "', '" + pago.getTipoPago() + "', '" + pago.getIdEmpresa() + "');";
				
			} else {
				sql = "INSERT INTO fin_payment("
						+ "fin_payment_id, ad_client_id, ad_org_id, created, createdby, "
						+ "updated, updatedby, isactive, isreceipt, c_bpartner_id, paymentdate, "
						+ "c_currency_id, amount, writeoffamt, fin_paymentmethod_id, documentno, "
						+ "referenceno, status, processed, processing, posted, description, "
						+ "fin_financial_account_id, c_doctype_id, c_project_id, c_campaign_id, "
						+ "c_activity_id, user1_id, user2_id, generated_credit, used_credit, "
						+ "createdbyalgorithm, finacc_txn_convert_rate, finacc_txn_amount, "
						+ "fin_rev_payment_id, c_costcenter_id, em_atecdp_deposito, em_co_nro_cheque, "
						+ "em_co_nombre_banco, em_aprm_process_payment, "
						+ "em_aprm_reconcile_payment, em_aprm_add_scheduledpayments, em_aprm_executepayment, "
						+ "em_aprm_reversepayment)"
						+ "VALUES ("
						+ "'" + finPaymentId + "', '" + this.auxInvoice.getAdClientId() + "', '" + this.auxInvoice.getAdOrgId() + "', now(), '100', "
						+ "now(), '100', 'Y', 'Y', '" + this.auxInvoice.getcBpartnerId() + "', (SELECT ci.dateinvoiced FROM c_invoice AS ci WHERE ci.c_invoice_id = '" + this.auxInvoice.getcInvoiceId() + "'), "
						+ "'100', '" + pago.getAmount() + "', '0', '" + pago.getFinPaymentmethodId() + "', '" + this.auxInvoice.getDocumentno() + "', "
						+ "null, 'RPR', 'N', 'N', 'N', '" + pago.getReferencia() + "Factura N� : ' || '" + this.auxInvoice.getDocumentno() + "', "
						+ "(SELECT fin_financial_account_id FROM fin_financial_account AS ffa WHERE upper(ffa.name) LIKE '%CAJA%' AND ffa.ad_org_id IN (SELECT ao.ad_org_id " + "FROM ad_org As ao " + "WHERE isactive = 'Y' " + "AND em_co_nro_estab = '" + this.auxInvoice.getSucursal() + "' " + "AND em_co_punto_emision = '001' AND ao.ad_client_id = '" + this.auxInvoice.getAdClientId() + "' LIMIT 1) "
						+ "AND ffa.ad_client_id = '" + this.auxInvoice.getAdClientId() + "' LIMIT 1), (SELECT c_doctype_id FROM c_doctype AS cd WHERE (upper(cd.name) LIKE '%COMPROBANTE DE INGRESO%' OR upper(cd.name) LIKE '%COBRO%') AND cd.ad_client_id = '" + this.auxInvoice.getAdClientId() + "' LIMIT 1), null, null, "
						+ "null, null, null, '0', '0', "
						+ "'N', '1', '" + pago.getAmount() + "', "
						+ "null, null, null, null, "
						+ "null, 'RE', "
						+ "'N', 'N', 'N', "
						+ "'N');";
			}
			
			registro = statement.executeUpdate(sql);
			
			sql = "UPDATE fin_payment_schedule SET fin_paymentmethod_id = '" + pago.getFinPaymentmethodId() + "' WHERE c_invoice_id = '" + this.auxInvoice.getcInvoiceId() + "';";

			statement.executeUpdate(sql);
			
			sql = "UPDATE c_invoice SET fin_paymentmethod_id = '" + pago.getFinPaymentmethodId() + "' WHERE c_invoice_id = '" + this.auxInvoice.getcInvoiceId() + "';";
			
			statement.executeUpdate(sql);
			
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (statement != null) statement.close(); } catch (Exception e) {};
		}
		
		if (registro > 0) {
			return finPaymentId;
		}else{
			return null;
		}
	}
	
	public String putFinPaymentDetailId(Pago pago, Connection connection) {
		Statement statement = null;
		String sql;
		int registro = 0;
		
		String finPaymentDetailId = UUID.randomUUID().toString().replace("-", "").toUpperCase();
		
		try {
			statement = connection.createStatement();
			
			sql = "INSERT INTO fin_payment_detail("
					+ "fin_payment_detail_id, ad_client_id, ad_org_id, created, createdby, "
					+ "updated, updatedby, fin_payment_id, amount, refund, isactive, "
					+ "writeoffamt, c_glitem_id, isprepayment, em_no_pago_line_id) "
					+ "VALUES ("
					+ "'" + finPaymentDetailId + "', '" + this.auxInvoice.getAdClientId() + "', '" + this.auxInvoice.getAdOrgId() + "', now(), '100', "
					+ "now(), '100', '" + pago.getFinPaymentId() + "', '" + pago.getAmount() + "', 'N', 'Y', "
					+ "'0', null, 'N', null);";
			
			registro = statement.executeUpdate(sql);
			
			sql = "UPDATE fin_payment "
					+ "SET amount = (SELECT sum(amount) "
					+ "FROM fin_payment_detail "
					+ "WHERE fin_payment_id = '" + pago.getFinPaymentId() + "'), finacc_txn_amount = ("
					+ "SELECT sum(amount) FROM fin_payment_detail "
					+ "WHERE fin_payment_id = '" + pago.getFinPaymentId() + "') "
					+ "WHERE fin_payment_id = '" + pago.getFinPaymentId() + "'";
			
			statement.executeUpdate(sql);
			
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (statement != null) statement.close(); } catch (Exception e) {};
		}
		
		if (registro > 0) {
			return finPaymentDetailId;
		}else{
			return null;
		}
	}
	
	public boolean procesarDocumento(JSONObject datos, List<InvoiceLine> lineas, Connection connection) {
		Statement statement = null;
		Statement statement2 = null; 
		String sql;
		int registro = 0;
		String pInstanceId = UUID.randomUUID().toString().replace("-", "").toUpperCase();
		String cProcessId = "";
		ResultSet rs = null;
		ResultSet rs2 = null;
		
		try {
			statement = connection.createStatement();
			statement2 = connection.createStatement();
			
			if (Double.valueOf(this.auxInvoice.getTotal()) > 0) {
				sql = "SELECT ad_process_id "
						+ "FROM ad_process "
						+ "WHERE upper(procedurename) = upper('c_invoice_post0') AND upper(name) = upper('Process Invoice');"; 
				rs = statement.executeQuery(sql);
				
				while (rs.next()) {
					cProcessId = rs.getString("ad_process_id");
				}

				rs.close();
				
				sql = "INSERT INTO ad_pinstance(ad_pinstance_id, ad_process_id, record_id, isprocessing, created, "
						+ "ad_user_id, updated, result, errormsg, ad_client_id, "
						+ "ad_org_id, createdby, updatedby, isactive) "
						+ "VALUES ('" + pInstanceId + "', '" + cProcessId + "', '" + this.auxInvoice.getcInvoiceId() +"', 'N', now(), "
						+ "'0', now(),'0','', '" + this.auxInvoice.getAdClientId() + "', "
						+ "'" + this.auxInvoice.getAdOrgId() + "', '0', '0', 'Y');";
				
				registro = statement.executeUpdate(sql);
				
				if (registro > 0) {
					rs = null;
					sql = "SELECT C_INVOICE.ISSOTRX AS v_IsSOTrx, "
							+ "C_INVOICE.c_bpartner_id AS v_BPartner_ID, "
							+ "C_INVOICE.EM_Co_Nro_Estab AS v_num_establecimiento, "
							+ "C_INVOICE.EM_Co_Punto_Emision AS v_punto_emision, "
							+ "C_INVOICE.c_bpartner_id AS v_cliente "
							+ "FROM C_INVOICE "
							+ "WHERE C_INVOICE_ID = '" + this.auxInvoice.getcInvoiceId() + "';";
					
					rs = statement.executeQuery(sql);
					
					while(rs.next()) {
						this.auxInvoice.setV_BPartner_ID(rs.getString("v_BPartner_ID"));
					}
					
					rs = null;
					sql = "SELECT CASE WHEN (m.ISSOTRX='Y') THEN customer_blocking  ELSE vendor_blocking END AS v_bpartner_blocked, "
							+ "CASE WHEN (m.ISSOTRX='Y') THEN so_invoice_blocking ELSE po_invoice_blocking END AS v_invoiceBlocking, "
							+ "bp.name AS v_bpartner_name, "
							+ "m.DocAction AS v_DocAction, "
							+ "m.DocStatus AS v_DocStatus, "
							+ "m.UpdatedBy AS v_UpdatedBy, "
							+ "m.AD_Org_ID AS v_Org_ID, "
							+ "m.DateAcct AS v_DateAcct, "
							+ "m.C_DocTypeTarget_ID AS v_DocTypeTarget_ID, "
							+ "pl.istaxincluded AS v_istaxincluded, "
							+ "m.ISSOTRX AS v_IsSOTrx, "
							+ "m.AD_Client_ID AS v_Client_ID, "
							+ "m.DocumentNo AS v_DocumentNo, "
							+ "m.AD_User_ID AS v_BPartner_User_ID "
							+ "FROM C_INVOICE m "
							+ "INNER JOIN C_BPartner bp ON (m.c_bpartner_id=bp.c_bpartner_id) "
							+ "INNER JOIN m_pricelist pl ON m.m_pricelist_id = pl.m_pricelist_id "
							+ "WHERE m.C_INVOICE_ID='" + this.auxInvoice.getcInvoiceId() + "' "
							+ "AND m.C_BPARTNER_ID='" + this.auxInvoice.getV_BPartner_ID() + "';";
					
					rs = statement.executeQuery(sql);
					
					while(rs.next()) {
						this.auxInvoice.setV_DocAction(rs.getString("v_DocAction"));
						this.auxInvoice.setV_DocStatus(rs.getString("v_DocStatus"));
						this.auxInvoice.setV_bpartner_blocked(rs.getString("v_bpartner_blocked"));
						this.auxInvoice.setV_invoiceBlocking(rs.getString("v_invoiceBlocking"));
						this.auxInvoice.setV_UpdatedBy(rs.getString("v_UpdatedBy"));
						this.auxInvoice.setV_Org_ID(rs.getString("v_Org_ID"));
						this.auxInvoice.setV_DateAcct(rs.getString("v_DateAcct"));
						this.auxInvoice.setV_DocTypeTarget_ID(rs.getString("v_DocTypeTarget_ID"));
						this.auxInvoice.setV_istaxincluded(rs.getString("v_istaxincluded"));
						this.auxInvoice.setV_IsSOTrx(rs.getString("v_IsSOTrx"));
						this.auxInvoice.setV_Client_ID(rs.getString("v_Client_ID"));
						this.auxInvoice.setV_DocumentNo(rs.getString("v_DocumentNo"));
						this.auxInvoice.setV_BPartner_User_ID(rs.getString("v_BPartner_User_ID"));
					}
					
					rs = null;
					sql = "SELECT count(*) as nrodocumentos "
							+ "FROM c_invoice ci "
							+ "WHERE ci.documentno = '" + this.auxInvoice.getV_DocumentNo() + "' "
							+ "AND ci.c_doctype_id IN (SELECT c_doctype_id FROM c_invoice WHERE c_invoice_id = '" + this.auxInvoice.getcInvoiceId() + "') "
							+ "AND ci.em_co_punto_emision IN (SELECT em_co_punto_emision FROM c_invoice WHERE c_invoice_id = '" + this.auxInvoice.getcInvoiceId() + "');";
					rs = statement.executeQuery(sql);
					
					while(rs.next()) {
						this.auxInvoice.setV_nrodumentos(rs.getInt("nrodocumentos"));
					}
					
					rs = null;
					sql = "SELECT COUNT(*) AS v_count "
							+ "FROM C_INVOICE "
							+ "WHERE C_INVOICE_ID='" + this.auxInvoice.getcInvoiceId() + "' "
							+ "AND (EXISTS (SELECT 1 "
							+ "FROM C_INVOICELINE "
							+ "WHERE C_INVOICE_ID='" + this.auxInvoice.getcInvoiceId() + "') "
							+ "OR EXISTS (SELECT 1 "
							+ "FROM C_INVOICETAX "
							+ "WHERE C_INVOICE_ID='" + this.auxInvoice.getcInvoiceId() + "'));";
					rs = statement.executeQuery(sql);
					
					while(rs.next()) {
						this.auxInvoice.setV_count(rs.getInt("v_count"));
					}
					
					rs = null;
					sql = "SELECT COUNT(*) AS v_Count_2 "
							+ "FROM C_INVOICE I, C_INVOICELINE IL "
							+ "WHERE I.C_INVOICE_ID = IL.C_INVOICE_ID "
							+ "AND AD_ISORGINCLUDED(IL.AD_Org_ID, I.AD_Org_ID, I.AD_Client_ID) = -1 "
							+ "AND I.C_INVOICE_ID = '" + this.auxInvoice.getcInvoiceId() + "';";
					rs = statement.executeQuery(sql);
					
					while(rs.next()) {
						this.auxInvoice.setV_Count_2(rs.getInt("v_Count_2"));
					}
					
					rs = null;
					sql = " SELECT COUNT(*) AS v_Count_3 "
							+ "FROM C_INVOICE C, "
							+ "C_DOCTYPE "
							+ "WHERE C_DOCTYPE.DocBaseType IN ( "
							+ "select docbasetype "
							+ "from c_doctype "
							+ "where ad_table_id='318' "
							+ "and isactive='Y' "
							+ "and ad_client_id=C.AD_Client_ID) "
							+ "AND C_DOCTYPE.IsSOTrx=C.ISSOTRX "
							+ "AND Ad_Isorgincluded(C.AD_Org_ID,C_DOCTYPE.AD_Org_ID, C.AD_Client_ID) <> -1 "
							+ "AND C.C_DOCTYPETARGET_ID = C_DOCTYPE.C_DOCTYPE_ID "
							+ "AND C.C_INVOICE_ID = '" + this.auxInvoice.getcInvoiceId() + "';";
					rs = statement.executeQuery(sql);
					
					while(rs.next()) {
						this.auxInvoice.setV_Count_3(rs.getInt("v_Count_3"));
					}
					
					rs = null;
					
					sql = "SELECT AD_Org.IsReady AS v_is_ready, Ad_OrgType.IsTransactionsAllowed AS v_is_tr_allow "
							+ "FROM C_INVOICE, AD_Org, AD_OrgType "
							+ "WHERE AD_Org.AD_Org_ID=C_INVOICE.AD_Org_ID "
							+ "AND AD_Org.AD_OrgType_ID=AD_OrgType.AD_OrgType_ID "
							+ "AND C_INVOICE.C_INVOICE_ID='" + this.auxInvoice.getcInvoiceId() + "';";
					rs = statement.executeQuery(sql);
					
					while(rs.next()) {
						this.auxInvoice.setV_is_ready(rs.getString("v_is_ready"));
						this.auxInvoice.setV_is_tr_allow(rs.getString("v_is_tr_allow"));
					}
					
					rs = null;
					sql = "SELECT AD_ORG_CHK_DOCUMENTS('C_INVOICE', 'C_INVOICELINE', '" + this.auxInvoice.getcInvoiceId() + "', 'C_INVOICE_ID', 'C_INVOICE_ID') AS v_is_included "
							+ "FROM dual;";
					rs = statement.executeQuery(sql);
					
					while(rs.next()) {
						this.auxInvoice.setV_is_included(rs.getInt("v_is_included"));
					}
					
					rs = null;
					
					sql = "SELECT AD_OrgType.IsAcctLegalEntity AS v_isacctle "
							+ "FROM AD_OrgType, AD_Org "
							+ "WHERE AD_Org.AD_OrgType_ID = AD_OrgType.AD_OrgType_ID "
							+ "AND AD_Org.AD_Org_ID=(SELECT AD_GET_DOC_LE_BU('C_INVOICE', '" + this.auxInvoice.getcInvoiceId() + "', 'C_INVOICE_ID', 'LE') AS v_org_bule_id "
							+ "FROM DUAL LIMIT 1)";
					rs = statement.executeQuery(sql);
					
					while(rs.next()) {
						this.auxInvoice.setV_isacctle(rs.getString("v_isacctle"));
					}
					
					rs = null;
					sql = "SELECT C_CHK_OPEN_PERIOD("
							+ "'" + this.auxInvoice.getV_Org_ID() + "' "
							+ ", date('" + this.auxInvoice.getV_DateAcct() + "') "
							+ ", NULL, '" + this.auxInvoice.getV_DocTypeTarget_ID() + "') AS v_available_period "
							+ "FROM DUAL";
					rs = statement.executeQuery(sql);
					
					while(rs.next()) {
						this.auxInvoice.setV_available_period(rs.getInt("v_available_period"));
					}
					
					rs = null;
					
					sql = "SELECT COUNT(*) AS v_count_4 "
							+ "FROM C_INVOICE c, C_BPARTNER bp "
							+ "WHERE c.C_BPARTNER_ID=bp.C_BPARTNER_ID "
							+ "AND Ad_Isorgincluded(c.AD_ORG_ID, bp.AD_ORG_ID, bp.AD_CLIENT_ID)=-1 "
							+ "AND c.C_Invoice_ID='" + this.auxInvoice.getcInvoiceId() + "';";
					rs = statement.executeQuery(sql);
					
					while(rs.next()) {
						this.auxInvoice.setV_count_4(rs.getInt("v_count_4"));
					}
					
					if (this.auxInvoice.getV_nrodumentos() > 1) {
						mensajeError = "Ya existe un documento con Nro de Documento, ";
					}else if (this.auxInvoice.getV_DocAction().equals("CO") && 
							this.auxInvoice.getV_bpartner_blocked().equals("Y") && 
							this.auxInvoice.getV_invoiceBlocking().equals("Y")) {
						mensajeError = "El tercero esta bloqueado, ";
					}else if (this.auxInvoice.getV_DocStatus().equals("VO") || 
							this.auxInvoice.getV_DocStatus().equals("CL") || 
							this.auxInvoice.getV_DocStatus().equals("RE")) {
						mensajeError = "Documento ya contabilizado, ";
					}else if (this.auxInvoice.getV_count() == 0) {
						mensajeError = "El documento no tiene lineas, ";
					}else if (this.auxInvoice.getV_Count_2() > 0) {
						mensajeError = "NotCorrectOrgLines, ";
					}else if (this.auxInvoice.getV_Count_3() == 0) {
						mensajeError = "NotCorrectOrgDoctypeInvoice, ";
					}else if (this.auxInvoice.getV_is_ready().equals("N")) {
						mensajeError = "OrgHeaderNotReady, ";
					}else if (this.auxInvoice.getV_is_tr_allow().equals("N")) {
						mensajeError = "OrgHeaderNotTransAllowed, ";
					}else if (this.auxInvoice.getV_is_included() == -1) {
						mensajeError = "LinesAndHeaderDifferentLEorBU, ";
					}else if (this.auxInvoice.getV_isacctle().equals("Y") && this.auxInvoice.getV_available_period() != 1) {
						mensajeError = "PeriodNotAvailable, ";
					}else if (this.auxInvoice.getV_count_4() > 0) {
						mensajeError = "NotCorrectOrgBpartnerInvoice, ";
					}else{
						sql = "UPDATE C_INVOICE "
								+ "SET Processing='Y', "
								+ "Updated=TO_DATE(NOW()), "
								+ "UpdatedBy='" + this.auxInvoice.getV_UpdatedBy() + "' "
								+ "WHERE C_Invoice_ID='" + this.auxInvoice.getcInvoiceId() + "';";
						statement.executeUpdate(sql);
						
						rs = null;
						sql = "SELECT c_invoiceline_ID "
								+ "FROM C_INVOICELINE l "
								+ "WHERE l.C_Invoice_ID='" + this.auxInvoice.getcInvoiceId() + "' "
								+ "AND l.IsActive='Y' "
								+ "AND l.explode='N' "
								+ "AND EXISTS "
								+ "(SELECT * "
								+ "FROM M_PRODUCT p "
								+ "WHERE l.M_Product_ID=p.M_Product_ID "
								+ "AND p.IsBOM='Y' "
								+ "AND p.IsStocked='N' "
								+ "AND p.productType='I') "
								+ "ORDER BY l.Line;";
						rs = statement.executeQuery(sql);
						
						while(rs.next()) {
							sql = "SELECT M_INVEXPLODEBOMNOTSTOCK(null, '" + rs.getString("c_invoiceline_ID") + "') "
									+ "FROM dual;";
							statement2.executeQuery(sql);
						}
						
						if (this.auxInvoice.getV_DocAction().equals("CO")) {
							sql = "SELECT M_PROMOTION_CALCULATE('I', v_Record_ID, v_UpdatedBy) FROM dual;";
						}
						
						sql = "DELETE FROM C_INVOICELINETAX "
								+ "WHERE C_InvoiceLine_ID IN (SELECT C_InvoiceLine_ID from C_InvoiceLine WHERE C_Invoice_ID = '" + this.auxInvoice.getcInvoiceId() + "');";
						statement.executeUpdate(sql);
						
						sql = "DELETE FROM C_INVOICETAX "
								+ "WHERE C_Invoice_ID='" + this.auxInvoice.getcInvoiceId() + "' AND Recalculate = 'Y'";
						statement.executeUpdate(sql);
						
						rs = null;
						sql = "SELECT C_INVOICETAX_ID "
								+ "FROM C_INVOICETAX "
								+ "WHERE C_Invoice_ID='" + this.auxInvoice.getcInvoiceId() + "' "
								+ "AND Recalculate = 'N'";
						rs = statement.executeQuery(sql);
						int auxline = 0;
						
						while(rs.next()) {
							auxline = auxline + 10;
							sql = "UPDATE C_INVOICETAX SET line='" + auxline + "' WHERE C_INVOICETAX_ID= '" + rs.getString("C_INVOICETAX_ID") + "';";
						}
						
						rs = null;
						sql = "SELECT l.C_InvoiceLine_ID, l.C_Tax_ID, i.IsTaxIncluded, "
								+ "COALESCE(SUM(l.LineNetAmt),0) + COALESCE(SUM(l.ChargeAmt), 0) AS LineNetAmt, "
								+ "COALESCE(SUM(l.TaxBaseAmt), 0) + COALESCE(SUM(l.ChargeAmt), 0) AS TaxBaseAmt, "
								+ "COALESCE(SUM(i.ChargeAmt), 0) AS HeaderNet, "
								+ "t.Rate, t.IsSummary, c.StdPrecision, t.Cascade, t.BaseAmount, "
								+ "l.line_gross_amount, m.value "
								+ "FROM C_INVOICE i, C_INVOICELINE l, C_TAX t, C_CURRENCY c, m_product m "
								+ "WHERE i.C_Invoice_ID=l.C_Invoice_ID "
								+ "AND i.C_Invoice_ID='" + this.auxInvoice.getcInvoiceId() + "' "
								+ "AND l.C_Tax_ID=t.C_Tax_ID "
								+ "AND i.C_Currency_ID=c.C_Currency_ID "
								+ "AND l.m_product_id = m.m_product_id "
								+ "GROUP BY l.C_InvoiceLine_ID, l.C_Tax_ID,i.IsTaxIncluded, t.Rate, t.IsSummary, "
								+ "c.StdPrecision, t.Cascade, t.BaseAmount, l.line_gross_amount, m.value "
								+ "ORDER BY 4 DESC;";
						rs = statement.executeQuery(sql);
						
						Float v_TotalLines = (float) 0;
						boolean HeaderNotAdded = true;
						
						
						while(rs.next()) {
							Float xTaxBaseAmt = rs.getFloat("LineNetAmt");
						    v_TotalLines = v_TotalLines + xTaxBaseAmt;
						    
						    if (HeaderNotAdded) {
						    	HeaderNotAdded = false;
						        xTaxBaseAmt = xTaxBaseAmt + rs.getFloat("HeaderNet");
						    }
						    
						    sql = "SELECT C_INVOICELINETAX_INSERT('" + this.auxInvoice.getV_Org_ID() + "', "
						    		+ "'" + this.auxInvoice.getcInvoiceId() + "', "
						    		+ "'" + rs.getString("C_InvoiceLine_ID") + "', "
						    		+ "'" + this.auxInvoice.getV_UpdatedBy() + "', "
						    		+ "'" + rs.getString("C_Tax_ID") + "', "
						    		+ "'" + rs.getString("C_Tax_ID") + "', "
						    		+ "'" + rs.getString("LineNetAmt") + "', "
						    		+ "'" + rs.getString("TaxBaseAmt") + "', "
						    		+ "'" + rs.getString("StdPrecision") + "') "
						    		+ "FROM dual;";
						    statement2.executeQuery(sql);
						    
						    if (this.auxInvoice.getV_istaxincluded().equals("Y")) {
						    	sql = "PERFORM C_INVOICELINETAX_ROUNDING("
						    			+ "'" + rs.getString("c_invoiceline_id") + "', "
						    			+ "'" + rs.getString("line_gross_amount") + "', "
						    			+ "'" + rs.getString("linenetamt") + "');";
						    	statement2.executeQuery(sql);
						    }
						}
						
						Float withholdamount = (float) 0;
						
						if (this.auxInvoice.getV_IsSOTrx().equals("N")) {
							rs = null;
							sql = "SELECT c_getwithholding(v_record_id) AS withholdamount "
									+ "FROM dual;";
							
							rs = statement.executeQuery(sql);
							
							while(rs.next()) {
								withholdamount = rs.getFloat("withholdamount");
							}
						}
						
						Float v_TaxNoRecalculable = (float) 0;
						Float v_grandtotal = (float) 0;
						
						/**
						 * Modificaci�n de valores
						 * **/
						
						rs = null;
						sql = "SELECT distinct(cilt.c_tax_id) AS c_tax_id "
								+ "FROM c_invoicelinetax cilt "
								+ "INNER JOIN c_invoiceline cil ON (cil.c_invoiceline_id = cilt.c_invoiceline_id) "
								+ "WHERE cilt.c_invoice_id = '" + this.auxInvoice.getcInvoiceId() + "';"; 
						
						rs = statement.executeQuery(sql);
						
						while (rs.next()) {
							rs2 = null;
							
							sql = "SELECT sum(cilt.taxamt) AS v_impuesto "
									+ "FROM c_invoicelinetax cilt "
									+ "WHERE cilt.c_invoice_id = '" + this.auxInvoice.getcInvoiceId() + "' "
									+ "AND cilt.c_tax_id = '" + rs.getString("c_tax_id") + "';";
							
							rs2 = statement2.executeQuery(sql);
							
							String auxVImpuesto = "0";
							
							while (rs2.next()) {
								auxVImpuesto = rs2.getString("v_impuesto");
							}
							
							sql = "UPDATE c_invoicetax "
									+ "SET taxamt = '" + auxVImpuesto + "' "
									+ "WHERE c_invoice_id='" + this.auxInvoice.getcInvoiceId() + "' "
									+ "AND c_tax_id = '" + rs.getString("c_tax_id") + "';";
							
							statement2.executeUpdate(sql);
							
							rs2 = null;
						}
						
						/**Modificaci�n de Valores de impuesto**/
						
						rs = null;
						sql = "SELECT cilt.c_tax_id, cilt.taxamt, cil.m_product_id, cilt.c_invoicelinetax_id "
								+ "FROM c_invoicelinetax cilt "
								+ "INNER JOIN c_invoiceline cil ON (cil.c_invoiceline_id = cilt.c_invoiceline_id) "
								+ "WHERE cilt.c_invoice_id = '" + this.auxInvoice.getcInvoiceId() + "';"; 
						
						rs = statement.executeQuery(sql);
						
						while (rs.next()) {
							for (int i=0; i < lineas.size(); i++) {
								if (rs.getString("c_tax_id").equals(lineas.get(i).getcTaxId()) && rs.getString("m_product_id").equals(lineas.get(i).getmProductId())) {
									sql = "UPDATE c_invoicelinetax "
											+ "SET taxamt = '" + lineas.get(i).getValor() + "' "
											+ "WHERE c_invoicelinetax_id = '" + rs.getString("c_invoicelinetax_id") + "';";
									
									statement2.executeUpdate(sql);
									
									rs2 = null;
									
									sql = "SELECT sum(cilt.taxamt) AS v_impuesto "
											+ "FROM c_invoicelinetax cilt "
											+ "WHERE cilt.c_invoice_id ='" + this.auxInvoice.getcInvoiceId() + "' "
											+ "AND cilt.c_tax_id = '" + rs.getString("c_tax_id") + "';";
									
									rs2 = statement2.executeQuery(sql);
									
									String auxVImpuesto = "0";
									
									while (rs2.next()) {
										auxVImpuesto = rs2.getString("v_impuesto");
									}
									
									sql = "UPDATE c_invoicetax "
											+ "SET taxamt = '" + auxVImpuesto + "' "
											+ "WHERE c_invoice_id='" + this.auxInvoice.getcInvoiceId() + "' "
											+ "AND c_tax_id = '" + rs.getString("c_tax_id") + "';";
									
									statement2.executeUpdate(sql);
									
									lineas.remove(i);
									break;
								}
							}
						}
						
						/******************************************************************************************/
						
						if (this.auxInvoice.getV_istaxincluded().equals("Y")) {
							rs = null;
							sql = "SELECT COALESCE(SUM(TaxAmt),0) AS v_TaxNoRecalculable "
									+ "FROM C_INVOICETAX "
									+ "WHERE C_Invoice_ID = '" + this.auxInvoice.getcInvoiceId() + "' "
									+ "AND Recalculate = 'N';";
							
							rs = statement.executeQuery(sql);
							
							while(rs.next()) {
								v_TaxNoRecalculable = rs.getFloat("v_TaxNoRecalculable");
							}
							
							rs = null;
							sql = "SELECT COALESCE(SUM(line_gross_amount), 0) AS v_grandtotal "
									+ "FROM c_invoiceline "
									+ "WHERE c_invoice_id = '" + this.auxInvoice.getcInvoiceId() + "';";
							rs = statement.executeQuery(sql);
							
							while(rs.next()) {
								v_grandtotal = rs.getFloat("v_grandtotal");
							}
							
					        v_grandtotal = v_grandtotal + v_TaxNoRecalculable;
						}else{
							rs.close();
							rs = null;
							sql = "SELECT COALESCE(SUM(TAXAMT), 0) AS v_grandtotal "
									+ "FROM C_INVOICETAX "
									+ "WHERE C_INVOICE_ID = '" + this.auxInvoice.getcInvoiceId() + "';";
							rs = statement.executeQuery(sql);
							
							while(rs.next()) {
								v_grandtotal = rs.getFloat("v_grandtotal");
							}
							
							v_grandtotal = v_grandtotal + v_TotalLines;
						}
						
						sql = "UPDATE C_INVOICE "
								+ "SET TotalLines= '" + v_TotalLines + "', "
								+ "GrandTotal='" + v_grandtotal + "', "
								+ "withholdingamount = '" + withholdamount + "' "
								+ "WHERE C_Invoice_ID='" + this.auxInvoice.getcInvoiceId() + "';";
						statement.executeUpdate(sql);
						
						if (this.auxInvoice.getV_istaxincluded().equals("Y")) {
							sql = "SELECT C_INVOICETAX_ROUNDING('" + this.auxInvoice.getcInvoiceId() + "', "
									+ "'" + (v_grandtotal - v_TaxNoRecalculable) + "', "
									+ "'" + v_TotalLines + "') "
									+ "FROM dual;";
							statement.executeQuery(sql);
						}
						
						sql = "UPDATE C_INVOICELINE "
								+ "SET AD_Client_ID='" + this.auxInvoice.getV_Client_ID() + "' "
								+ "WHERE C_Invoice_ID='" + this.auxInvoice.getcInvoiceId() + "' "
								+ "AND AD_Client_ID<>'" + this.auxInvoice.getV_Client_ID() + "';";
						statement.executeUpdate(sql);
						
						sql = "UPDATE C_BPARTNER "
								+ "SET FirstSale=date('" + this.auxInvoice.getV_DateAcct() + "') "
								+ "WHERE C_BPartner_ID='" + this.auxInvoice.getV_BPartner_ID() + "' "
								+ "AND FirstSale IS NULL;";
						statement.executeUpdate(sql);
						
						if (this.auxInvoice.getV_IsSOTrx().equals("Y")) {
							sql = "SELECT C_BP_SOCREDITUSED_REFRESH('" + this.auxInvoice.getV_BPartner_ID() + "') "
									+ "FROM dual;";
						}
						
						sql = "UPDATE C_INVOICE "
								+ "SET DocStatus='CO', "
								+ "Processed='Y', "
								+ "DocAction='RE', "
								+ "Updated=TO_DATE(NOW()), "
								+ "UpdatedBy='" + this.auxInvoice.getV_UpdatedBy() + "' "
								+ "WHERE C_Invoice_ID='" + this.auxInvoice.getcInvoiceId() + "';"; 
						statement.executeUpdate(sql);
						
						sql = "UPDATE C_INVOICE "
								+ "SET Processing='N', "
								+ "Updated=TO_DATE(NOW()), "
								+ "UpdatedBy='" + this.auxInvoice.getV_UpdatedBy() + "' "
								+ "WHERE C_Invoice_ID='" + this.auxInvoice.getcInvoiceId() + "';";
						statement.executeUpdate(sql);
						
						String v_ep_instance = UUID.randomUUID().toString().replace("-", "").toUpperCase();
						String v_extension_point_id = "CBE7DD2E561E4D3D8257ECEA5F19687F";
						
						sql = "SELECT AD_EP_INSTANCE_PARA_INSERT('" + v_ep_instance + "', '" + v_extension_point_id + "', 'Record_ID', '" + this.auxInvoice.getcInvoiceId() + "', NULL, NULL, NULL, NULL, NULL, NULL) "
								+ "FROM dual;";
						statement.executeQuery(sql);
						
						sql = "SELECT AD_EP_INSTANCE_PARA_INSERT('" + v_ep_instance + "', '" + v_extension_point_id + "', 'DocAction', '" + this.auxInvoice.getV_DocAction() + "', NULL, NULL, NULL, NULL, NULL, NULL) "
								+ "FROM dual;";
						statement.executeQuery(sql);
						
						sql = "SELECT AD_EP_INSTANCE_PARA_INSERT('" + v_ep_instance + "', '" + v_extension_point_id + "', 'User', '" + this.auxInvoice.getV_UpdatedBy() + "', NULL, NULL, NULL, NULL, NULL, NULL) "
								+ "FROM dual;";
						statement.executeQuery(sql);
						
						sql = "SELECT AD_EP_INSTANCE_PARA_INSERT('" + v_ep_instance + "', '" + v_extension_point_id + "', 'Message', NULL, NULL, NULL, NULL, NULL, NULL, '') "
								+ "FROM dual;";
						statement.executeQuery(sql);
						
						sql = "SELECT AD_EP_INSTANCE_PARA_INSERT('" + v_ep_instance + "', '" + v_extension_point_id + "', 'Result', NULL, NULL, '1', NULL, NULL, NULL, NULL) "
								+ "FROM dual;";
						statement.executeQuery(sql);
						
						sql = "SELECT AD_EXTENSION_POINT_HANDLER('" + v_ep_instance + "', '" + v_extension_point_id + "')  "
								+ "FROM dual;";
						statement.executeQuery(sql);
					}
					
					rs = null;
				    registro = 0;
				    sql = "SELECT amount, count(*) AS total "
				    		+ "FROM fin_payment_schedule "
				    		+ "WHERE c_invoice_id = '" + this.auxInvoice.getcInvoiceId() + "' "
				    		+ "GROUP BY amount;";
				    rs = statement.executeQuery(sql);
				    
				    String aux2amount = "0";
				    while (rs.next()) {
				    	registro = Integer.valueOf(rs.getString("total"));
				    	aux2amount = rs.getString("amount");
					}
				    
				    log.info("C_Invoice_ID: " + this.auxInvoice.getcInvoiceId());
				    log.info("Total Factura Openbravo:" + aux2amount);
				    log.info("Total Factura Aloha: " + this.auxInvoice.getTotal());
				    
				    rs.close();
				    rs = null;
					
				    /*if (Double.valueOf(aux2amount) != Double.valueOf(this.auxInvoice.getTotal())) {
				    	log.warn("Los valores no cordinan en la factura");
				    	this.mensajeError = "El documento no tiene el total en funcion del subtotal e iva, verificar los valores de la factura";
				    	registro = 0;
				    }*/
				    
				    if (registro == 0) {
				    	if (mensajeError.equals("")) {
				    		sql = "SELECT errormsg "
				    				+ "FROM ad_pinstance "
				    				+ "WHERE ad_pinstance_id = '" + pInstanceId + "';";
				    		
						    rs = statement.executeQuery(sql);
						    String auxmens = "";
						    
						    while (rs.next()) {
						    	auxmens = rs.getString("errormsg");
							}
						    
						    rs.close();
						    rs = null;
						    
						    this.mensajeError = "Documento no completado: " + auxmens + ", ";
				    	}
				    	registro = 0;
				    }
				}else{
					this.mensajeError = "No se pudo crear la instancia, ";
				}
			} else {
				registro = 1;
			}
			
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (statement != null) statement.close(); } catch (Exception e) {};
			try { if (statement2 != null) statement2.close(); } catch (Exception e) {};
		}
		
		if (registro > 0) {
			return true;
		}else{
			this.mensajeError = this.mensajeError + "Documento no ingresado a la BDD (Comuniquese con el administrador)";
			return false;
		}
	}
	
	public boolean updateFinPaymentScheduledetailId(Pago pago, Connection connection) {
		Statement statement = null;
		ResultSet rs;
		String sql;
		int registro = 0;
		double auxTotalPago = 0;
		double auxTotalInvoice = 0;
		
		try {
			statement = connection.createStatement();
			
			sql = "UPDATE fin_payment_scheduledetail "
					+ "SET amount = '" + pago.getAmount() + "' , fin_payment_detail_id = '" + pago.getFinPaymentDetailId() + "' "
					+ "WHERE fin_payment_schedule_invoice = '" + pago.getFinPaymentScheduleId() + "' "
					+ "AND fin_payment_detail_id is null;";
			
			registro = statement.executeUpdate(sql);
			
			sql = "SELECT sum(fps.amount) AS totalPago "
					+ "FROM fin_payment_scheduledetail fps "
					+ "WHERE fps.fin_payment_schedule_invoice = '" + pago.getFinPaymentScheduleId() + "' "
					+ "AND fps.fin_payment_detail_id is not null;";
			
			rs = statement.executeQuery(sql);
			
			while (rs.next()) {
				auxTotalPago = rs.getDouble("totalPago");
			}
			
			rs = null;
			
			sql = "SELECT ci.grandtotal AS totalInvoice "
					+ "FROM c_invoice ci "
					+ "WHERE ci.c_invoice_id = '" + this.auxInvoice.getcInvoiceId() + "'";
			
			rs = statement.executeQuery(sql);
			
			while (rs.next()) {
				auxTotalInvoice = rs.getDouble("totalInvoice");
			}
			
			rs = null;
			
			sql = "UPDATE fin_payment_schedule "
					+ "SET paidamt = (SELECT sum(amount) "
					+ "FROM fin_payment_scheduledetail as fps WHERE fps.fin_payment_schedule_invoice = '" + pago.getFinPaymentScheduleId() + "'), outstandingamt = amount - (SELECT sum(amount) FROM fin_payment_scheduledetail as fps WHERE fps.fin_payment_schedule_invoice = '" + pago.getFinPaymentScheduleId() + "') WHERE fin_payment_schedule_id = '" + pago.getFinPaymentScheduleId() + "';";
			
			statement.executeUpdate(sql);
			
			if (auxTotalInvoice != auxTotalPago) {
				
				sql = "INSERT INTO fin_payment_scheduledetail(" 
						+ "fin_payment_scheduledetail_id, ad_client_id, ad_org_id, created, " 
						+ "createdby, updated, updatedby, fin_payment_detail_id, fin_payment_schedule_order, "
						+ "fin_payment_schedule_invoice, amount, isactive, writeoffamt, "
						+ "iscanceled, c_bpartner_id, c_activity_id, m_product_id, c_campaign_id, "
						+ "c_project_id, c_salesregion_id, c_costcenter_id, user1_id, user2_id, "
						+ "doubtfuldebt_amount) "
						+ "VALUES ("
						+ "get_uuid(), '" + this.auxInvoice.getAdClientId() + "', '" + this.auxInvoice.getAdOrgId() + "', now(), "
						+ "'100', now(), '100', null, null, "
						+ "'" + pago.getFinPaymentScheduleId() + "', '" + (auxTotalInvoice - auxTotalPago) + "', 'Y', '0', "
						+ "'N', null, null, null, null, "
						+ "null, null, null, null, null, "
						+ "'0');";
				
				statement.executeUpdate(sql);
			}
			
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (statement != null) statement.close(); } catch (Exception e) {};
		}
		
		if (registro > 0) {
			return true;
		}else{
			return false;
		}
	}
	
	public boolean procesarPago(Pago pago, Connection connection) {
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			
			sql = "UPDATE fin_payment "
					+ "SET status = 'RDNC', processed = 'Y', em_aprm_process_payment = 'RE' "
					+ "WHERE fin_payment_id = '" + pago.getFinPaymentId() + "';";
			
			registro = statement.executeUpdate(sql);
			
			sql = "INSERT INTO fin_finacc_transaction("
					+ "fin_finacc_transaction_id, ad_client_id, ad_org_id, created, "
					+ "createdby, updated, updatedby, isactive, c_currency_id, fin_financial_account_id, "
					+ "line, fin_payment_id, dateacct, c_glitem_id, status, paymentamt, "
					+ "depositamt, processed, processing, posted, c_project_id, c_campaign_id, "
					+ "c_activity_id, user1_id, user2_id, trxtype, statementdate, description, "
					+ "fin_reconciliation_id, createdbyalgorithm, foreign_currency_id, "
					+ "foreign_convert_rate, foreign_amount, c_bpartner_id, m_product_id, "
					+ "c_salesregion_id, c_costcenter_id, em_aprm_delete, em_aprm_modify) "
					+ "VALUES (get_uuid(), '" + this.auxInvoice.getAdClientId() + "', '" + this.auxInvoice.getAdOrgId() + "', now(), "
					+ "'100', now(), '100', 'Y', '100', (SELECT fin_financial_account_id "
					+ "FROM fin_financial_account AS ffa "
					+ "WHERE upper(ffa.name) LIKE '%CAJA%' AND ffa.ad_org_id IN (SELECT ao.ad_org_id " + "FROM ad_org As ao " + "WHERE isactive = 'Y' " + "AND em_co_nro_estab = '" + this.auxInvoice.getSucursal() + "' " + "AND em_co_punto_emision = '001' AND ao.ad_client_id = '" + this.auxInvoice.getAdClientId() + "' LIMIT 1) AND ffa.ad_client_id = '" + this.auxInvoice.getAdClientId() + "' LIMIT 1), "
					+ "(SELECT coalesce((max(line) + 10),10) "
					+ "FROM fin_finacc_transaction "
					+ "WHERE fin_financial_account_id IN (SELECT fin_financial_account_id "
					+ "FROM fin_financial_account AS ffa "
					+ "WHERE upper(ffa.name) LIKE '%CAJA%' AND ffa.ad_org_id IN (SELECT ao.ad_org_id " + "FROM ad_org As ao " + "WHERE isactive = 'Y' " + "AND em_co_nro_estab = '" + this.auxInvoice.getSucursal() + "' " + "AND em_co_punto_emision = '001' AND ao.ad_client_id = '" + this.auxInvoice.getAdClientId() + "' LIMIT 1) AND ffa.ad_client_id = '" + this.auxInvoice.getAdClientId() + "' LIMIT 1)), '" + pago.getFinPaymentId() + "', (SELECT ci.dateinvoiced FROM c_invoice AS ci WHERE ci.c_invoice_id = '" + this.auxInvoice.getcInvoiceId() + "'), null, 'RDNC', 0, "
					+ "(SELECT amount FROM fin_payment WHERE fin_payment_id = '" + pago.getFinPaymentId() + "'), 'Y', 'N', 'N', null, null, "
					+ "null, null, null, 'BPD', (SELECT ci.dateinvoiced FROM c_invoice AS ci WHERE ci.c_invoice_id = '" + this.auxInvoice.getcInvoiceId() + "'), (SELECT description FROM fin_payment WHERE fin_payment_id = '" + pago.getFinPaymentId() + "'), "
					+ "null, 'N', null, "
					+ "null, null, (SELECT c_bpartner_id FROM fin_payment WHERE fin_payment_id = '" + pago.getFinPaymentId() + "'), null, "
					+ "null, null, 'N', 'N');";
			
			statement.executeUpdate(sql);
			
			sql = "UPDATE fin_financial_account SET currentbalance = (SELECT sum(depositamt) - sum(paymentamt) "
					+ "FROM fin_finacc_transaction "
					+ "WHERE fin_financial_account_id IN (SELECT fin_financial_account_id FROM fin_financial_account AS ffa WHERE upper(ffa.name) LIKE '%CAJA%' AND ffa.ad_org_id IN (SELECT ao.ad_org_id " + "FROM ad_org As ao " + "WHERE isactive = 'Y' " + "AND em_co_nro_estab = '" + this.auxInvoice.getSucursal() + "' " + "AND em_co_punto_emision = '001' AND ao.ad_client_id = '" + this.auxInvoice.getAdClientId() + "' LIMIT 1) AND ffa.ad_client_id = '" + this.auxInvoice.getAdClientId() + "' LIMIT 1)) " 
					+ "WHERE fin_financial_account_id IN (SELECT fin_financial_account_id FROM fin_financial_account AS ffa WHERE upper(ffa.name) LIKE '%CAJA%' AND ffa.ad_org_id IN (SELECT ao.ad_org_id " + "FROM ad_org As ao " + "WHERE isactive = 'Y' " + "AND em_co_nro_estab = '" + this.auxInvoice.getSucursal() + "' " + "AND em_co_punto_emision = '001' AND ao.ad_client_id = '" + this.auxInvoice.getAdClientId() + "' LIMIT 1) AND ffa.ad_client_id = '" + this.auxInvoice.getAdClientId() + "' LIMIT 1);";
			
			statement.executeUpdate(sql);
			
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (statement != null) statement.close(); } catch (Exception e) {};
		}
		
		if (registro > 0) {
			return true;
		}else{
			return false;
		}
	}
	
	public boolean terminarFactura(boolean isPagado, double totalPago, Connection connection) {
		// TODO Auto-generated method stub
		Statement statement = null;
		String sql;
		int registro = 0;
		
		try {
			statement = connection.createStatement();
			sql = "UPDATE c_invoice SET description = '" + this.auxInvoice.getDescription() + "', "
					+ "ispaid = '" + (isPagado? "Y" : "N") + "', "
					+ "totalpaid = round('" + totalPago + "',2), "
					+ "outstandingamt = round(grandtotal - round('" + totalPago + "',2),2) , "
					+ "dueamt = round(grandtotal - round('" + totalPago + "',2),2), finalsettlement = now(), daysoutstanding = '12', percentageoverdue = '100' "
					+ "WHERE c_invoice_id = '" + this.auxInvoice.getcInvoiceId() + "';";
			registro = statement.executeUpdate(sql);
		} catch (SQLException ex) {
			// TODO Auto-generated catch block
			log.warn(ex.getMessage());
			this.exception = true;
		} finally {
			try { if (statement != null) statement.close(); } catch (Exception e) {};
		}
		
		if (registro > 0) {
			return true;
		}else{
			return false;
		}
	}
}
